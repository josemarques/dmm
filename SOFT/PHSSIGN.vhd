Library ieee; 
use ieee.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;

use ieee.numeric_std.all;
--use ieee.std_logic_arith.all; 
--use ieee.std_logic_signed.all;

--use ieee.std_logic_signed.all;

--Propagation delay measurement logic
 ENTITY PHSSIGN IS
 PORT (	F0: IN std_logic;
			F1: IN std_logic;
			NEGPHS: OUT std_logic
 );
 END ENTITY;

ARCHITECTURE SECPHSSNG OF PHSSIGN IS
SIGNAL TMPNEG0 : std_logic;
SIGNAL TMPNEG1 : std_logic;
 BEGIN

 NEGPHS<=TMPNEG0 OR TMPNEG1;
 
PROCESS(F0)
BEGIN
IF(F0'EVENT and F0='1') THEN
IF(F1='0')THEN
TMPNEG0<='0';
ELSE
TMPNEG0<='1';
END IF;
END IF;
END PROCESS;

PROCESS(F1)
BEGIN
IF(F1'EVENT and F1='1') THEN
IF(F0='1')THEN
TMPNEG1<='0';
ELSE
TMPNEG1<='1';
END IF;
END IF;
END PROCESS;

END ARCHITECTURE;