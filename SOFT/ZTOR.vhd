--Zeros to ones relation
Library ieee; 
use ieee.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;

use ieee.numeric_std.all;
--use ieee.std_logic_arith.all; 
--use ieee.std_logic_signed.all;

--Propagation delay reference combining logic
 ENTITY ZTOR IS
 generic(N: integer :=32);--Number of bits of count
 PORT ( 	CLK: IN std_logic;
			SIG: IN std_logic_vector(N-1 downto 0);
			REL: OUT std_logic_vector(N-1 downto 0)
 );
 END ENTITY;

ARCHITECTURE OPERATIONAL_LOGIC_ARCH OF ZTOR IS
signal RESULT : std_logic_vector(N-1 downto 0);--Result calculation
signal BUFF : std_logic_vector(N-1 downto 0);--Result calculation

BEGIN

PROCESS(CLK)
variable ONES : integer range 0 to N;
BEGIN

IF(CLK'EVENT and CLK='1') THEN

BUFF<=SIG;
ONES:=0;

FOR i in N-1 downto 0 LOOP
--ONES := ONES + to_integer(unsigned'('0' & SIG(i)));
IF(BUFF(i)='1')THEN
ONES:=ONES+1;
END IF;
END LOOP;
RESULT<=std_logic_vector(to_unsigned(ONES,RESULT'LENGTH));

END IF;

END PROCESS;


REL<=RESULT;

END ARCHITECTURE;