Library ieee; 
use ieee.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;

use ieee.numeric_std.all;
--use ieee.std_logic_arith.all; 
--use ieee.std_logic_signed.all;

--Propagation delay pattern
 ENTITY PROPDPATT IS
 generic(N: integer :=32);--Number of bits of the detector
 PORT (PATT: OUT std_logic_vector(N-1 downto 0)
 );
 END ENTITY;

ARCHITECTURE PROPDPATT_GEN OF PROPDPATT IS
signal SIGPATT : std_logic_vector(N-1 downto 0);--Pattern
 BEGIN
 PATT<=SIGPATT;
--NOTDELAYS
SIGPATT(N-1)<= '1' ; 
NOTDELAYS : for i in N-2 downto 0 generate
    SIGPATT(i) <= NOT SIGPATT(i+1);
end generate; 

END ARCHITECTURE;