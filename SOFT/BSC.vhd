LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY BSC IS --Bit stream counter
	GENERIC (NBS: integer := 32;--Number of bits in the bits stream
				NBA:integer:=32--Number of bits of the accumulator
	);
  
	PORT(	CLK:IN std_logic;	
			BITSTR: IN std_logic_vector (NBS-1 DOWNTO 0);
			RST: IN std_logic;
			ADD: OUT std_logic_vector (NBA-1 DOWNTO 0)
	);

			END BSC;

ARCHITECTURE BINARYCNT OF BSC IS
  SIGNAL SUMA: std_logic_vector (NBA-1 DOWNTO 0):=(OTHERS=>'0');
  SIGNAL SUMB: std_logic_vector (NBA-1 DOWNTO 0):=(OTHERS=>'0');
  
   
BEGIN

PROCESS(RST) BEGIN
	IF (RST'EVENT AND RST= '1') THEN
		ADD<=std_logic_vector(unsigned(SUMA)-unsigned(SUMB));
		SUMB<=SUMA;
	END IF;
END PROCESS;
	
PROCESS(CLK) BEGIN
	IF (CLK'EVENT AND CLK= '1') THEN
		PARTIALSUMS : FOR s in (NBS)-1 DOWNTO 0 LOOP
			IF(BITSTR(s)='1') THEN
				SUMA<=std_logic_vector(unsigned(SUMA)+1);
			END IF;
		END LOOP;
	END IF;
END PROCESS;


END BINARYCNT;