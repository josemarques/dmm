-- Copyright (C) 2023  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 22.1std.1 Build 917 02/14/2023 SC Lite Edition"

-- DATE "01/11/2024 15:55:14"

-- 
-- Device: Altera EP4CE6E22I7 Package TQFP144
-- 

-- 
-- This VHDL file should be used for Questa Intel FPGA (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCEO~	=>  Location: PIN_101,	 I/O Standard: 2.5 V,	 Current Strength: 8mA


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	DMM IS
    PORT (
	RESET : OUT std_logic;
	RF : IN std_logic;
	POWER : OUT std_logic_vector(31 DOWNTO 0)
	);
END DMM;

-- Design Ports Information
-- RESET	=>  Location: PIN_87,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[31]	=>  Location: PIN_49,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[30]	=>  Location: PIN_50,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[29]	=>  Location: PIN_44,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[28]	=>  Location: PIN_121,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[27]	=>  Location: PIN_32,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[26]	=>  Location: PIN_28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[25]	=>  Location: PIN_33,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[24]	=>  Location: PIN_132,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[23]	=>  Location: PIN_124,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[22]	=>  Location: PIN_127,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[21]	=>  Location: PIN_46,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[20]	=>  Location: PIN_133,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[19]	=>  Location: PIN_43,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[18]	=>  Location: PIN_59,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[17]	=>  Location: PIN_136,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[16]	=>  Location: PIN_54,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[15]	=>  Location: PIN_137,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[14]	=>  Location: PIN_125,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[13]	=>  Location: PIN_30,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[12]	=>  Location: PIN_10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[11]	=>  Location: PIN_53,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[10]	=>  Location: PIN_135,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[9]	=>  Location: PIN_51,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[8]	=>  Location: PIN_11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[7]	=>  Location: PIN_52,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[6]	=>  Location: PIN_31,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[5]	=>  Location: PIN_55,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[4]	=>  Location: PIN_128,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[3]	=>  Location: PIN_60,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[2]	=>  Location: PIN_129,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[1]	=>  Location: PIN_126,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- POWER[0]	=>  Location: PIN_58,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- RF	=>  Location: PIN_23,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF DMM IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_RESET : std_logic;
SIGNAL ww_RF : std_logic;
SIGNAL ww_POWER : std_logic_vector(31 DOWNTO 0);
SIGNAL \inst7|DIVD~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \RF~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \RESET~output_o\ : std_logic;
SIGNAL \POWER[31]~output_o\ : std_logic;
SIGNAL \POWER[30]~output_o\ : std_logic;
SIGNAL \POWER[29]~output_o\ : std_logic;
SIGNAL \POWER[28]~output_o\ : std_logic;
SIGNAL \POWER[27]~output_o\ : std_logic;
SIGNAL \POWER[26]~output_o\ : std_logic;
SIGNAL \POWER[25]~output_o\ : std_logic;
SIGNAL \POWER[24]~output_o\ : std_logic;
SIGNAL \POWER[23]~output_o\ : std_logic;
SIGNAL \POWER[22]~output_o\ : std_logic;
SIGNAL \POWER[21]~output_o\ : std_logic;
SIGNAL \POWER[20]~output_o\ : std_logic;
SIGNAL \POWER[19]~output_o\ : std_logic;
SIGNAL \POWER[18]~output_o\ : std_logic;
SIGNAL \POWER[17]~output_o\ : std_logic;
SIGNAL \POWER[16]~output_o\ : std_logic;
SIGNAL \POWER[15]~output_o\ : std_logic;
SIGNAL \POWER[14]~output_o\ : std_logic;
SIGNAL \POWER[13]~output_o\ : std_logic;
SIGNAL \POWER[12]~output_o\ : std_logic;
SIGNAL \POWER[11]~output_o\ : std_logic;
SIGNAL \POWER[10]~output_o\ : std_logic;
SIGNAL \POWER[9]~output_o\ : std_logic;
SIGNAL \POWER[8]~output_o\ : std_logic;
SIGNAL \POWER[7]~output_o\ : std_logic;
SIGNAL \POWER[6]~output_o\ : std_logic;
SIGNAL \POWER[5]~output_o\ : std_logic;
SIGNAL \POWER[4]~output_o\ : std_logic;
SIGNAL \POWER[3]~output_o\ : std_logic;
SIGNAL \POWER[2]~output_o\ : std_logic;
SIGNAL \POWER[1]~output_o\ : std_logic;
SIGNAL \POWER[0]~output_o\ : std_logic;
SIGNAL \RF~input_o\ : std_logic;
SIGNAL \RF~inputclkctrl_outclk\ : std_logic;
SIGNAL \inst7|COUNT[0]~7_combout\ : std_logic;
SIGNAL \inst7|LessThan0~0_combout\ : std_logic;
SIGNAL \inst7|LessThan0~1_combout\ : std_logic;
SIGNAL \inst7|COUNT[0]~8\ : std_logic;
SIGNAL \inst7|COUNT[1]~9_combout\ : std_logic;
SIGNAL \inst7|COUNT[1]~10\ : std_logic;
SIGNAL \inst7|COUNT[2]~11_combout\ : std_logic;
SIGNAL \inst7|COUNT[2]~12\ : std_logic;
SIGNAL \inst7|COUNT[3]~13_combout\ : std_logic;
SIGNAL \inst7|COUNT[3]~14\ : std_logic;
SIGNAL \inst7|COUNT[4]~15_combout\ : std_logic;
SIGNAL \inst7|COUNT[4]~16\ : std_logic;
SIGNAL \inst7|COUNT[5]~17_combout\ : std_logic;
SIGNAL \inst7|COUNT[5]~18\ : std_logic;
SIGNAL \inst7|COUNT[6]~19_combout\ : std_logic;
SIGNAL \inst7|DIVD~0_combout\ : std_logic;
SIGNAL \inst7|DIVD~feeder_combout\ : std_logic;
SIGNAL \inst7|DIVD~q\ : std_logic;
SIGNAL \inst7|DIVD~clkctrl_outclk\ : std_logic;
SIGNAL \inst4|SUMA[0]~32_combout\ : std_logic;
SIGNAL \inst4|SUMA~101_combout\ : std_logic;
SIGNAL \inst4|SUMA~102_combout\ : std_logic;
SIGNAL \inst4|SUMA~103_combout\ : std_logic;
SIGNAL \inst4|SUMA~96_combout\ : std_logic;
SIGNAL \inst4|SUMA~98_combout\ : std_logic;
SIGNAL \inst4|SUMA~97_combout\ : std_logic;
SIGNAL \inst4|SUMA~99_combout\ : std_logic;
SIGNAL \inst4|SUMA~100_combout\ : std_logic;
SIGNAL \inst4|SUMA~104_combout\ : std_logic;
SIGNAL \inst4|SUMA~105_combout\ : std_logic;
SIGNAL \inst4|SUMA~106_combout\ : std_logic;
SIGNAL \inst4|SUMA[0]~33\ : std_logic;
SIGNAL \inst4|SUMA[1]~34_combout\ : std_logic;
SIGNAL \inst4|SUMA[1]~35\ : std_logic;
SIGNAL \inst4|SUMA[2]~36_combout\ : std_logic;
SIGNAL \inst4|SUMA[2]~37\ : std_logic;
SIGNAL \inst4|SUMA[3]~38_combout\ : std_logic;
SIGNAL \inst4|SUMA[3]~39\ : std_logic;
SIGNAL \inst4|SUMA[4]~40_combout\ : std_logic;
SIGNAL \inst4|SUMA[4]~41\ : std_logic;
SIGNAL \inst4|SUMA[5]~42_combout\ : std_logic;
SIGNAL \inst4|SUMA[5]~43\ : std_logic;
SIGNAL \inst4|SUMA[6]~44_combout\ : std_logic;
SIGNAL \inst4|SUMA[6]~45\ : std_logic;
SIGNAL \inst4|SUMA[7]~46_combout\ : std_logic;
SIGNAL \inst4|SUMA[7]~47\ : std_logic;
SIGNAL \inst4|SUMA[8]~48_combout\ : std_logic;
SIGNAL \inst4|SUMA[8]~49\ : std_logic;
SIGNAL \inst4|SUMA[9]~50_combout\ : std_logic;
SIGNAL \inst4|SUMA[9]~51\ : std_logic;
SIGNAL \inst4|SUMA[10]~52_combout\ : std_logic;
SIGNAL \inst4|SUMA[10]~53\ : std_logic;
SIGNAL \inst4|SUMA[11]~54_combout\ : std_logic;
SIGNAL \inst4|SUMA[11]~55\ : std_logic;
SIGNAL \inst4|SUMA[12]~56_combout\ : std_logic;
SIGNAL \inst4|SUMA[12]~57\ : std_logic;
SIGNAL \inst4|SUMA[13]~58_combout\ : std_logic;
SIGNAL \inst4|SUMA[13]~59\ : std_logic;
SIGNAL \inst4|SUMA[14]~60_combout\ : std_logic;
SIGNAL \inst4|SUMA[14]~61\ : std_logic;
SIGNAL \inst4|SUMA[15]~62_combout\ : std_logic;
SIGNAL \inst4|SUMA[15]~63\ : std_logic;
SIGNAL \inst4|SUMA[16]~64_combout\ : std_logic;
SIGNAL \inst4|SUMA[16]~65\ : std_logic;
SIGNAL \inst4|SUMA[17]~66_combout\ : std_logic;
SIGNAL \inst4|SUMA[17]~67\ : std_logic;
SIGNAL \inst4|SUMA[18]~68_combout\ : std_logic;
SIGNAL \inst4|SUMA[18]~69\ : std_logic;
SIGNAL \inst4|SUMA[19]~70_combout\ : std_logic;
SIGNAL \inst4|SUMA[19]~71\ : std_logic;
SIGNAL \inst4|SUMA[20]~72_combout\ : std_logic;
SIGNAL \inst4|SUMA[20]~73\ : std_logic;
SIGNAL \inst4|SUMA[21]~74_combout\ : std_logic;
SIGNAL \inst4|SUMA[21]~75\ : std_logic;
SIGNAL \inst4|SUMA[22]~76_combout\ : std_logic;
SIGNAL \inst4|SUMA[22]~77\ : std_logic;
SIGNAL \inst4|SUMA[23]~78_combout\ : std_logic;
SIGNAL \inst4|SUMA[23]~79\ : std_logic;
SIGNAL \inst4|SUMA[24]~80_combout\ : std_logic;
SIGNAL \inst4|SUMA[24]~81\ : std_logic;
SIGNAL \inst4|SUMA[25]~82_combout\ : std_logic;
SIGNAL \inst4|SUMA[25]~83\ : std_logic;
SIGNAL \inst4|SUMA[26]~84_combout\ : std_logic;
SIGNAL \inst4|SUMA[26]~85\ : std_logic;
SIGNAL \inst4|SUMA[27]~86_combout\ : std_logic;
SIGNAL \inst4|SUMA[27]~87\ : std_logic;
SIGNAL \inst4|SUMA[28]~88_combout\ : std_logic;
SIGNAL \inst4|SUMA[28]~89\ : std_logic;
SIGNAL \inst4|SUMA[29]~90_combout\ : std_logic;
SIGNAL \inst4|SUMA[29]~91\ : std_logic;
SIGNAL \inst4|SUMA[30]~92_combout\ : std_logic;
SIGNAL \inst4|SUMA[30]~93\ : std_logic;
SIGNAL \inst4|SUMA[31]~94_combout\ : std_logic;
SIGNAL \inst4|SUMB[31]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[30]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[29]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[28]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[27]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[26]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[25]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[24]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[23]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[22]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[21]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[20]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[19]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[18]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[17]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[16]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[15]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[14]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[13]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[12]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[11]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[10]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[9]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[8]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[7]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[6]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[5]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[4]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[3]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[2]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[1]~feeder_combout\ : std_logic;
SIGNAL \inst4|SUMB[0]~feeder_combout\ : std_logic;
SIGNAL \inst4|ADD[0]~33\ : std_logic;
SIGNAL \inst4|ADD[1]~35\ : std_logic;
SIGNAL \inst4|ADD[2]~37\ : std_logic;
SIGNAL \inst4|ADD[3]~39\ : std_logic;
SIGNAL \inst4|ADD[4]~41\ : std_logic;
SIGNAL \inst4|ADD[5]~43\ : std_logic;
SIGNAL \inst4|ADD[6]~45\ : std_logic;
SIGNAL \inst4|ADD[7]~47\ : std_logic;
SIGNAL \inst4|ADD[8]~49\ : std_logic;
SIGNAL \inst4|ADD[9]~51\ : std_logic;
SIGNAL \inst4|ADD[10]~53\ : std_logic;
SIGNAL \inst4|ADD[11]~55\ : std_logic;
SIGNAL \inst4|ADD[12]~57\ : std_logic;
SIGNAL \inst4|ADD[13]~59\ : std_logic;
SIGNAL \inst4|ADD[14]~61\ : std_logic;
SIGNAL \inst4|ADD[15]~63\ : std_logic;
SIGNAL \inst4|ADD[16]~65\ : std_logic;
SIGNAL \inst4|ADD[17]~67\ : std_logic;
SIGNAL \inst4|ADD[18]~69\ : std_logic;
SIGNAL \inst4|ADD[19]~71\ : std_logic;
SIGNAL \inst4|ADD[20]~73\ : std_logic;
SIGNAL \inst4|ADD[21]~75\ : std_logic;
SIGNAL \inst4|ADD[22]~77\ : std_logic;
SIGNAL \inst4|ADD[23]~79\ : std_logic;
SIGNAL \inst4|ADD[24]~81\ : std_logic;
SIGNAL \inst4|ADD[25]~83\ : std_logic;
SIGNAL \inst4|ADD[26]~85\ : std_logic;
SIGNAL \inst4|ADD[27]~87\ : std_logic;
SIGNAL \inst4|ADD[28]~89\ : std_logic;
SIGNAL \inst4|ADD[29]~91\ : std_logic;
SIGNAL \inst4|ADD[30]~93\ : std_logic;
SIGNAL \inst4|ADD[31]~94_combout\ : std_logic;
SIGNAL \inst4|ADD[30]~92_combout\ : std_logic;
SIGNAL \inst4|ADD[29]~90_combout\ : std_logic;
SIGNAL \inst4|ADD[28]~88_combout\ : std_logic;
SIGNAL \inst4|ADD[27]~86_combout\ : std_logic;
SIGNAL \inst4|ADD[26]~84_combout\ : std_logic;
SIGNAL \inst4|ADD[25]~82_combout\ : std_logic;
SIGNAL \inst4|ADD[24]~80_combout\ : std_logic;
SIGNAL \inst4|ADD[23]~78_combout\ : std_logic;
SIGNAL \inst4|ADD[22]~76_combout\ : std_logic;
SIGNAL \inst4|ADD[21]~74_combout\ : std_logic;
SIGNAL \inst4|ADD[20]~72_combout\ : std_logic;
SIGNAL \inst4|ADD[19]~70_combout\ : std_logic;
SIGNAL \inst4|ADD[18]~68_combout\ : std_logic;
SIGNAL \inst4|ADD[17]~66_combout\ : std_logic;
SIGNAL \inst4|ADD[16]~64_combout\ : std_logic;
SIGNAL \inst4|ADD[15]~62_combout\ : std_logic;
SIGNAL \inst4|ADD[14]~60_combout\ : std_logic;
SIGNAL \inst4|ADD[13]~58_combout\ : std_logic;
SIGNAL \inst4|ADD[12]~56_combout\ : std_logic;
SIGNAL \inst4|ADD[11]~54_combout\ : std_logic;
SIGNAL \inst4|ADD[10]~52_combout\ : std_logic;
SIGNAL \inst4|ADD[9]~50_combout\ : std_logic;
SIGNAL \inst4|ADD[8]~48_combout\ : std_logic;
SIGNAL \inst4|ADD[7]~46_combout\ : std_logic;
SIGNAL \inst4|ADD[6]~44_combout\ : std_logic;
SIGNAL \inst4|ADD[5]~42_combout\ : std_logic;
SIGNAL \inst4|ADD[4]~40_combout\ : std_logic;
SIGNAL \inst4|ADD[3]~38_combout\ : std_logic;
SIGNAL \inst4|ADD[2]~36_combout\ : std_logic;
SIGNAL \inst4|ADD[1]~34_combout\ : std_logic;
SIGNAL \inst4|ADD[0]~32_combout\ : std_logic;
SIGNAL \inst6|STEMP\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \inst4|SUMB\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \inst4|ADD\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \inst4|SUMA\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \inst7|COUNT\ : std_logic_vector(6 DOWNTO 0);

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

RESET <= ww_RESET;
ww_RF <= RF;
POWER <= ww_POWER;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\inst7|DIVD~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \inst7|DIVD~q\);

\RF~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \RF~input_o\);
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X34_Y10_N9
\RESET~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst7|DIVD~q\,
	devoe => ww_devoe,
	o => \RESET~output_o\);

-- Location: IOOBUF_X13_Y0_N16
\POWER[31]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(31),
	devoe => ww_devoe,
	o => \POWER[31]~output_o\);

-- Location: IOOBUF_X13_Y0_N2
\POWER[30]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(30),
	devoe => ww_devoe,
	o => \POWER[30]~output_o\);

-- Location: IOOBUF_X5_Y0_N16
\POWER[29]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(29),
	devoe => ww_devoe,
	o => \POWER[29]~output_o\);

-- Location: IOOBUF_X23_Y24_N16
\POWER[28]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(28),
	devoe => ww_devoe,
	o => \POWER[28]~output_o\);

-- Location: IOOBUF_X0_Y6_N16
\POWER[27]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(27),
	devoe => ww_devoe,
	o => \POWER[27]~output_o\);

-- Location: IOOBUF_X0_Y9_N9
\POWER[26]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(26),
	devoe => ww_devoe,
	o => \POWER[26]~output_o\);

-- Location: IOOBUF_X0_Y6_N23
\POWER[25]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(25),
	devoe => ww_devoe,
	o => \POWER[25]~output_o\);

-- Location: IOOBUF_X13_Y24_N16
\POWER[24]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(24),
	devoe => ww_devoe,
	o => \POWER[24]~output_o\);

-- Location: IOOBUF_X18_Y24_N16
\POWER[23]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(23),
	devoe => ww_devoe,
	o => \POWER[23]~output_o\);

-- Location: IOOBUF_X16_Y24_N9
\POWER[22]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(22),
	devoe => ww_devoe,
	o => \POWER[22]~output_o\);

-- Location: IOOBUF_X7_Y0_N2
\POWER[21]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(21),
	devoe => ww_devoe,
	o => \POWER[21]~output_o\);

-- Location: IOOBUF_X13_Y24_N23
\POWER[20]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(20),
	devoe => ww_devoe,
	o => \POWER[20]~output_o\);

-- Location: IOOBUF_X5_Y0_N23
\POWER[19]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(19),
	devoe => ww_devoe,
	o => \POWER[19]~output_o\);

-- Location: IOOBUF_X23_Y0_N16
\POWER[18]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(18),
	devoe => ww_devoe,
	o => \POWER[18]~output_o\);

-- Location: IOOBUF_X9_Y24_N9
\POWER[17]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(17),
	devoe => ww_devoe,
	o => \POWER[17]~output_o\);

-- Location: IOOBUF_X18_Y0_N23
\POWER[16]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(16),
	devoe => ww_devoe,
	o => \POWER[16]~output_o\);

-- Location: IOOBUF_X7_Y24_N2
\POWER[15]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(15),
	devoe => ww_devoe,
	o => \POWER[15]~output_o\);

-- Location: IOOBUF_X18_Y24_N23
\POWER[14]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(14),
	devoe => ww_devoe,
	o => \POWER[14]~output_o\);

-- Location: IOOBUF_X0_Y8_N16
\POWER[13]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(13),
	devoe => ww_devoe,
	o => \POWER[13]~output_o\);

-- Location: IOOBUF_X0_Y18_N16
\POWER[12]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(12),
	devoe => ww_devoe,
	o => \POWER[12]~output_o\);

-- Location: IOOBUF_X16_Y0_N2
\POWER[11]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(11),
	devoe => ww_devoe,
	o => \POWER[11]~output_o\);

-- Location: IOOBUF_X11_Y24_N16
\POWER[10]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(10),
	devoe => ww_devoe,
	o => \POWER[10]~output_o\);

-- Location: IOOBUF_X16_Y0_N23
\POWER[9]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(9),
	devoe => ww_devoe,
	o => \POWER[9]~output_o\);

-- Location: IOOBUF_X0_Y18_N23
\POWER[8]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(8),
	devoe => ww_devoe,
	o => \POWER[8]~output_o\);

-- Location: IOOBUF_X16_Y0_N9
\POWER[7]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(7),
	devoe => ww_devoe,
	o => \POWER[7]~output_o\);

-- Location: IOOBUF_X0_Y7_N2
\POWER[6]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(6),
	devoe => ww_devoe,
	o => \POWER[6]~output_o\);

-- Location: IOOBUF_X18_Y0_N16
\POWER[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(5),
	devoe => ww_devoe,
	o => \POWER[5]~output_o\);

-- Location: IOOBUF_X16_Y24_N16
\POWER[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(4),
	devoe => ww_devoe,
	o => \POWER[4]~output_o\);

-- Location: IOOBUF_X23_Y0_N9
\POWER[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(3),
	devoe => ww_devoe,
	o => \POWER[3]~output_o\);

-- Location: IOOBUF_X16_Y24_N23
\POWER[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(2),
	devoe => ww_devoe,
	o => \POWER[2]~output_o\);

-- Location: IOOBUF_X16_Y24_N2
\POWER[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(1),
	devoe => ww_devoe,
	o => \POWER[1]~output_o\);

-- Location: IOOBUF_X21_Y0_N9
\POWER[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|ADD\(0),
	devoe => ww_devoe,
	o => \POWER[0]~output_o\);

-- Location: IOIBUF_X0_Y11_N8
\RF~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_RF,
	o => \RF~input_o\);

-- Location: CLKCTRL_G2
\RF~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \RF~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \RF~inputclkctrl_outclk\);

-- Location: LCCOMB_X33_Y11_N14
\inst7|COUNT[0]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|COUNT[0]~7_combout\ = \inst7|COUNT\(0) $ (VCC)
-- \inst7|COUNT[0]~8\ = CARRY(\inst7|COUNT\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst7|COUNT\(0),
	datad => VCC,
	combout => \inst7|COUNT[0]~7_combout\,
	cout => \inst7|COUNT[0]~8\);

-- Location: LCCOMB_X33_Y11_N28
\inst7|LessThan0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|LessThan0~0_combout\ = ((!\inst7|COUNT\(1) & (!\inst7|COUNT\(2) & !\inst7|COUNT\(3)))) # (!\inst7|COUNT\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|COUNT\(1),
	datab => \inst7|COUNT\(2),
	datac => \inst7|COUNT\(4),
	datad => \inst7|COUNT\(3),
	combout => \inst7|LessThan0~0_combout\);

-- Location: LCCOMB_X33_Y11_N30
\inst7|LessThan0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|LessThan0~1_combout\ = (\inst7|COUNT\(6)) # ((\inst7|COUNT\(5) & !\inst7|LessThan0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst7|COUNT\(5),
	datac => \inst7|COUNT\(6),
	datad => \inst7|LessThan0~0_combout\,
	combout => \inst7|LessThan0~1_combout\);

-- Location: FF_X33_Y11_N15
\inst7|COUNT[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst7|COUNT[0]~7_combout\,
	sclr => \inst7|LessThan0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT\(0));

-- Location: LCCOMB_X33_Y11_N16
\inst7|COUNT[1]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|COUNT[1]~9_combout\ = (\inst7|COUNT\(1) & (!\inst7|COUNT[0]~8\)) # (!\inst7|COUNT\(1) & ((\inst7|COUNT[0]~8\) # (GND)))
-- \inst7|COUNT[1]~10\ = CARRY((!\inst7|COUNT[0]~8\) # (!\inst7|COUNT\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|COUNT\(1),
	datad => VCC,
	cin => \inst7|COUNT[0]~8\,
	combout => \inst7|COUNT[1]~9_combout\,
	cout => \inst7|COUNT[1]~10\);

-- Location: FF_X33_Y11_N17
\inst7|COUNT[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst7|COUNT[1]~9_combout\,
	sclr => \inst7|LessThan0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT\(1));

-- Location: LCCOMB_X33_Y11_N18
\inst7|COUNT[2]~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|COUNT[2]~11_combout\ = (\inst7|COUNT\(2) & (\inst7|COUNT[1]~10\ $ (GND))) # (!\inst7|COUNT\(2) & (!\inst7|COUNT[1]~10\ & VCC))
-- \inst7|COUNT[2]~12\ = CARRY((\inst7|COUNT\(2) & !\inst7|COUNT[1]~10\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|COUNT\(2),
	datad => VCC,
	cin => \inst7|COUNT[1]~10\,
	combout => \inst7|COUNT[2]~11_combout\,
	cout => \inst7|COUNT[2]~12\);

-- Location: FF_X33_Y11_N19
\inst7|COUNT[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst7|COUNT[2]~11_combout\,
	sclr => \inst7|LessThan0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT\(2));

-- Location: LCCOMB_X33_Y11_N20
\inst7|COUNT[3]~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|COUNT[3]~13_combout\ = (\inst7|COUNT\(3) & (!\inst7|COUNT[2]~12\)) # (!\inst7|COUNT\(3) & ((\inst7|COUNT[2]~12\) # (GND)))
-- \inst7|COUNT[3]~14\ = CARRY((!\inst7|COUNT[2]~12\) # (!\inst7|COUNT\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|COUNT\(3),
	datad => VCC,
	cin => \inst7|COUNT[2]~12\,
	combout => \inst7|COUNT[3]~13_combout\,
	cout => \inst7|COUNT[3]~14\);

-- Location: FF_X33_Y11_N21
\inst7|COUNT[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst7|COUNT[3]~13_combout\,
	sclr => \inst7|LessThan0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT\(3));

-- Location: LCCOMB_X33_Y11_N22
\inst7|COUNT[4]~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|COUNT[4]~15_combout\ = (\inst7|COUNT\(4) & (\inst7|COUNT[3]~14\ $ (GND))) # (!\inst7|COUNT\(4) & (!\inst7|COUNT[3]~14\ & VCC))
-- \inst7|COUNT[4]~16\ = CARRY((\inst7|COUNT\(4) & !\inst7|COUNT[3]~14\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|COUNT\(4),
	datad => VCC,
	cin => \inst7|COUNT[3]~14\,
	combout => \inst7|COUNT[4]~15_combout\,
	cout => \inst7|COUNT[4]~16\);

-- Location: FF_X33_Y11_N23
\inst7|COUNT[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst7|COUNT[4]~15_combout\,
	sclr => \inst7|LessThan0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT\(4));

-- Location: LCCOMB_X33_Y11_N24
\inst7|COUNT[5]~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|COUNT[5]~17_combout\ = (\inst7|COUNT\(5) & (!\inst7|COUNT[4]~16\)) # (!\inst7|COUNT\(5) & ((\inst7|COUNT[4]~16\) # (GND)))
-- \inst7|COUNT[5]~18\ = CARRY((!\inst7|COUNT[4]~16\) # (!\inst7|COUNT\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst7|COUNT\(5),
	datad => VCC,
	cin => \inst7|COUNT[4]~16\,
	combout => \inst7|COUNT[5]~17_combout\,
	cout => \inst7|COUNT[5]~18\);

-- Location: FF_X33_Y11_N25
\inst7|COUNT[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst7|COUNT[5]~17_combout\,
	sclr => \inst7|LessThan0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT\(5));

-- Location: LCCOMB_X33_Y11_N26
\inst7|COUNT[6]~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|COUNT[6]~19_combout\ = \inst7|COUNT\(6) $ (!\inst7|COUNT[5]~18\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|COUNT\(6),
	cin => \inst7|COUNT[5]~18\,
	combout => \inst7|COUNT[6]~19_combout\);

-- Location: FF_X33_Y11_N27
\inst7|COUNT[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst7|COUNT[6]~19_combout\,
	sclr => \inst7|LessThan0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|COUNT\(6));

-- Location: LCCOMB_X33_Y11_N10
\inst7|DIVD~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|DIVD~0_combout\ = \inst7|DIVD~q\ $ (((\inst7|COUNT\(6)) # ((\inst7|COUNT\(5) & !\inst7|LessThan0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101000011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst7|COUNT\(6),
	datab => \inst7|COUNT\(5),
	datac => \inst7|DIVD~q\,
	datad => \inst7|LessThan0~0_combout\,
	combout => \inst7|DIVD~0_combout\);

-- Location: LCCOMB_X33_Y11_N12
\inst7|DIVD~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst7|DIVD~feeder_combout\ = \inst7|DIVD~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst7|DIVD~0_combout\,
	combout => \inst7|DIVD~feeder_combout\);

-- Location: FF_X33_Y11_N13
\inst7|DIVD\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst7|DIVD~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst7|DIVD~q\);

-- Location: CLKCTRL_G7
\inst7|DIVD~clkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \inst7|DIVD~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \inst7|DIVD~clkctrl_outclk\);

-- Location: LCCOMB_X12_Y11_N0
\inst4|SUMA[0]~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[0]~32_combout\ = \inst4|SUMA\(0) $ (VCC)
-- \inst4|SUMA[0]~33\ = CARRY(\inst4|SUMA\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(0),
	datad => VCC,
	combout => \inst4|SUMA[0]~32_combout\,
	cout => \inst4|SUMA[0]~33\);

-- Location: LCCOMB_X5_Y11_N12
\inst6|STEMP[31]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(31) = LCELL(!\RF~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \RF~input_o\,
	combout => \inst6|STEMP\(31));

-- Location: LCCOMB_X5_Y11_N8
\inst6|STEMP[30]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(30) = LCELL(!\inst6|STEMP\(31))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(31),
	combout => \inst6|STEMP\(30));

-- Location: LCCOMB_X5_Y11_N6
\inst6|STEMP[29]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(29) = LCELL(!\inst6|STEMP\(30))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst6|STEMP\(30),
	combout => \inst6|STEMP\(29));

-- Location: LCCOMB_X5_Y11_N26
\inst6|STEMP[28]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(28) = LCELL(!\inst6|STEMP\(29))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(29),
	combout => \inst6|STEMP\(28));

-- Location: LCCOMB_X5_Y11_N0
\inst6|STEMP[27]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(27) = LCELL(!\inst6|STEMP\(28))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst6|STEMP\(28),
	combout => \inst6|STEMP\(27));

-- Location: LCCOMB_X5_Y11_N28
\inst6|STEMP[26]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(26) = LCELL(!\inst6|STEMP\(27))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(27),
	combout => \inst6|STEMP\(26));

-- Location: LCCOMB_X5_Y11_N2
\inst6|STEMP[25]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(25) = LCELL(!\inst6|STEMP\(26))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(26),
	combout => \inst6|STEMP\(25));

-- Location: LCCOMB_X5_Y11_N18
\inst6|STEMP[24]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(24) = LCELL(!\inst6|STEMP\(25))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(25),
	combout => \inst6|STEMP\(24));

-- Location: LCCOMB_X5_Y11_N20
\inst6|STEMP[23]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(23) = LCELL(!\inst6|STEMP\(24))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(24),
	combout => \inst6|STEMP\(23));

-- Location: LCCOMB_X5_Y11_N16
\inst6|STEMP[22]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(22) = LCELL(!\inst6|STEMP\(23))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(23),
	combout => \inst6|STEMP\(22));

-- Location: LCCOMB_X5_Y11_N30
\inst6|STEMP[21]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(21) = LCELL(!\inst6|STEMP\(22))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(22),
	combout => \inst6|STEMP\(21));

-- Location: LCCOMB_X5_Y11_N14
\inst6|STEMP[20]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(20) = LCELL(!\inst6|STEMP\(21))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst6|STEMP\(21),
	combout => \inst6|STEMP\(20));

-- Location: LCCOMB_X5_Y11_N24
\inst6|STEMP[19]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(19) = LCELL(!\inst6|STEMP\(20))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst6|STEMP\(20),
	combout => \inst6|STEMP\(19));

-- Location: LCCOMB_X5_Y11_N4
\inst6|STEMP[18]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(18) = LCELL(!\inst6|STEMP\(19))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(19),
	combout => \inst6|STEMP\(18));

-- Location: LCCOMB_X5_Y11_N10
\inst6|STEMP[17]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(17) = LCELL(!\inst6|STEMP\(18))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst6|STEMP\(18),
	combout => \inst6|STEMP\(17));

-- Location: LCCOMB_X5_Y11_N22
\inst6|STEMP[16]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(16) = LCELL(!\inst6|STEMP\(17))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(17),
	combout => \inst6|STEMP\(16));

-- Location: LCCOMB_X10_Y11_N18
\inst6|STEMP[15]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(15) = LCELL(!\inst6|STEMP\(16))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(16),
	combout => \inst6|STEMP\(15));

-- Location: LCCOMB_X10_Y11_N2
\inst6|STEMP[14]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(14) = LCELL(!\inst6|STEMP\(15))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(15),
	combout => \inst6|STEMP\(14));

-- Location: LCCOMB_X10_Y11_N24
\inst6|STEMP[13]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(13) = LCELL(!\inst6|STEMP\(14))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(14),
	combout => \inst6|STEMP\(13));

-- Location: LCCOMB_X10_Y11_N12
\inst6|STEMP[12]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(12) = LCELL(!\inst6|STEMP\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(13),
	combout => \inst6|STEMP\(12));

-- Location: LCCOMB_X10_Y11_N10
\inst6|STEMP[11]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(11) = LCELL(!\inst6|STEMP\(12))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(12),
	combout => \inst6|STEMP\(11));

-- Location: LCCOMB_X10_Y11_N20
\inst6|STEMP[10]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(10) = LCELL(!\inst6|STEMP\(11))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(11),
	combout => \inst6|STEMP\(10));

-- Location: LCCOMB_X10_Y11_N6
\inst6|STEMP[9]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(9) = LCELL(!\inst6|STEMP\(10))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(10),
	combout => \inst6|STEMP\(9));

-- Location: LCCOMB_X10_Y11_N28
\inst4|SUMA~101\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA~101_combout\ = (\inst6|STEMP\(15) & (!\inst6|STEMP\(14) & (!\inst6|STEMP\(12) & \inst6|STEMP\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|STEMP\(15),
	datab => \inst6|STEMP\(14),
	datac => \inst6|STEMP\(12),
	datad => \inst6|STEMP\(13),
	combout => \inst4|SUMA~101_combout\);

-- Location: LCCOMB_X10_Y11_N30
\inst4|SUMA~102\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA~102_combout\ = (\inst6|STEMP\(11) & !\inst6|STEMP\(10))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst6|STEMP\(11),
	datad => \inst6|STEMP\(10),
	combout => \inst4|SUMA~102_combout\);

-- Location: LCCOMB_X10_Y11_N16
\inst6|STEMP[8]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(8) = LCELL(!\inst6|STEMP\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(9),
	combout => \inst6|STEMP\(8));

-- Location: LCCOMB_X10_Y11_N0
\inst4|SUMA~103\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA~103_combout\ = (\inst6|STEMP\(9) & (\inst4|SUMA~101_combout\ & (\inst4|SUMA~102_combout\ & !\inst6|STEMP\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|STEMP\(9),
	datab => \inst4|SUMA~101_combout\,
	datac => \inst4|SUMA~102_combout\,
	datad => \inst6|STEMP\(8),
	combout => \inst4|SUMA~103_combout\);

-- Location: LCCOMB_X6_Y11_N12
\inst4|SUMA~96\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA~96_combout\ = (\inst6|STEMP\(31) & (\inst6|STEMP\(29) & (!\inst6|STEMP\(28) & !\inst6|STEMP\(30))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|STEMP\(31),
	datab => \inst6|STEMP\(29),
	datac => \inst6|STEMP\(28),
	datad => \inst6|STEMP\(30),
	combout => \inst4|SUMA~96_combout\);

-- Location: LCCOMB_X6_Y11_N28
\inst4|SUMA~98\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA~98_combout\ = (!\inst6|STEMP\(22) & (\inst6|STEMP\(21) & (!\inst6|STEMP\(20) & \inst6|STEMP\(23))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|STEMP\(22),
	datab => \inst6|STEMP\(21),
	datac => \inst6|STEMP\(20),
	datad => \inst6|STEMP\(23),
	combout => \inst4|SUMA~98_combout\);

-- Location: LCCOMB_X6_Y11_N22
\inst4|SUMA~97\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA~97_combout\ = (\inst6|STEMP\(25) & (!\inst6|STEMP\(26) & (!\inst6|STEMP\(24) & \inst6|STEMP\(27))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|STEMP\(25),
	datab => \inst6|STEMP\(26),
	datac => \inst6|STEMP\(24),
	datad => \inst6|STEMP\(27),
	combout => \inst4|SUMA~97_combout\);

-- Location: LCCOMB_X6_Y11_N18
\inst4|SUMA~99\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA~99_combout\ = (\inst6|STEMP\(17) & (\inst6|STEMP\(19) & (!\inst6|STEMP\(18) & !\inst6|STEMP\(16))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|STEMP\(17),
	datab => \inst6|STEMP\(19),
	datac => \inst6|STEMP\(18),
	datad => \inst6|STEMP\(16),
	combout => \inst4|SUMA~99_combout\);

-- Location: LCCOMB_X6_Y11_N24
\inst4|SUMA~100\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA~100_combout\ = (\inst4|SUMA~96_combout\ & (\inst4|SUMA~98_combout\ & (\inst4|SUMA~97_combout\ & \inst4|SUMA~99_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA~96_combout\,
	datab => \inst4|SUMA~98_combout\,
	datac => \inst4|SUMA~97_combout\,
	datad => \inst4|SUMA~99_combout\,
	combout => \inst4|SUMA~100_combout\);

-- Location: LCCOMB_X10_Y11_N14
\inst6|STEMP[7]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(7) = LCELL(!\inst6|STEMP\(8))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(8),
	combout => \inst6|STEMP\(7));

-- Location: LCCOMB_X11_Y11_N24
\inst6|STEMP[6]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(6) = LCELL(!\inst6|STEMP\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(7),
	combout => \inst6|STEMP\(6));

-- Location: LCCOMB_X11_Y11_N18
\inst6|STEMP[5]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(5) = LCELL(!\inst6|STEMP\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(6),
	combout => \inst6|STEMP\(5));

-- Location: LCCOMB_X11_Y11_N2
\inst6|STEMP[4]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(4) = LCELL(!\inst6|STEMP\(5))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(5),
	combout => \inst6|STEMP\(4));

-- Location: LCCOMB_X11_Y11_N8
\inst4|SUMA~104\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA~104_combout\ = (\inst6|STEMP\(7) & (!\inst6|STEMP\(6) & (\inst6|STEMP\(5) & !\inst6|STEMP\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|STEMP\(7),
	datab => \inst6|STEMP\(6),
	datac => \inst6|STEMP\(5),
	datad => \inst6|STEMP\(4),
	combout => \inst4|SUMA~104_combout\);

-- Location: LCCOMB_X11_Y11_N12
\inst6|STEMP[3]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(3) = LCELL(!\inst6|STEMP\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(4),
	combout => \inst6|STEMP\(3));

-- Location: LCCOMB_X11_Y11_N20
\inst6|STEMP[2]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(2) = LCELL(!\inst6|STEMP\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(3),
	combout => \inst6|STEMP\(2));

-- Location: LCCOMB_X11_Y11_N26
\inst6|STEMP[1]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(1) = LCELL(!\inst6|STEMP\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst6|STEMP\(2),
	combout => \inst6|STEMP\(1));

-- Location: LCCOMB_X11_Y11_N6
\inst6|STEMP[0]\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst6|STEMP\(0) = LCELL(!\inst6|STEMP\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst6|STEMP\(1),
	combout => \inst6|STEMP\(0));

-- Location: LCCOMB_X11_Y11_N10
\inst4|SUMA~105\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA~105_combout\ = (\inst6|STEMP\(3) & (!\inst6|STEMP\(2) & (\inst6|STEMP\(1) & !\inst6|STEMP\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst6|STEMP\(3),
	datab => \inst6|STEMP\(2),
	datac => \inst6|STEMP\(1),
	datad => \inst6|STEMP\(0),
	combout => \inst4|SUMA~105_combout\);

-- Location: LCCOMB_X11_Y11_N28
\inst4|SUMA~106\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA~106_combout\ = (((!\inst4|SUMA~105_combout\) # (!\inst4|SUMA~104_combout\)) # (!\inst4|SUMA~100_combout\)) # (!\inst4|SUMA~103_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA~103_combout\,
	datab => \inst4|SUMA~100_combout\,
	datac => \inst4|SUMA~104_combout\,
	datad => \inst4|SUMA~105_combout\,
	combout => \inst4|SUMA~106_combout\);

-- Location: FF_X12_Y11_N1
\inst4|SUMA[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[0]~32_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(0));

-- Location: LCCOMB_X12_Y11_N2
\inst4|SUMA[1]~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[1]~34_combout\ = (\inst4|SUMA\(1) & (!\inst4|SUMA[0]~33\)) # (!\inst4|SUMA\(1) & ((\inst4|SUMA[0]~33\) # (GND)))
-- \inst4|SUMA[1]~35\ = CARRY((!\inst4|SUMA[0]~33\) # (!\inst4|SUMA\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(1),
	datad => VCC,
	cin => \inst4|SUMA[0]~33\,
	combout => \inst4|SUMA[1]~34_combout\,
	cout => \inst4|SUMA[1]~35\);

-- Location: FF_X12_Y11_N3
\inst4|SUMA[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[1]~34_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(1));

-- Location: LCCOMB_X12_Y11_N4
\inst4|SUMA[2]~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[2]~36_combout\ = (\inst4|SUMA\(2) & (\inst4|SUMA[1]~35\ $ (GND))) # (!\inst4|SUMA\(2) & (!\inst4|SUMA[1]~35\ & VCC))
-- \inst4|SUMA[2]~37\ = CARRY((\inst4|SUMA\(2) & !\inst4|SUMA[1]~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(2),
	datad => VCC,
	cin => \inst4|SUMA[1]~35\,
	combout => \inst4|SUMA[2]~36_combout\,
	cout => \inst4|SUMA[2]~37\);

-- Location: FF_X12_Y11_N5
\inst4|SUMA[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[2]~36_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(2));

-- Location: LCCOMB_X12_Y11_N6
\inst4|SUMA[3]~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[3]~38_combout\ = (\inst4|SUMA\(3) & (!\inst4|SUMA[2]~37\)) # (!\inst4|SUMA\(3) & ((\inst4|SUMA[2]~37\) # (GND)))
-- \inst4|SUMA[3]~39\ = CARRY((!\inst4|SUMA[2]~37\) # (!\inst4|SUMA\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(3),
	datad => VCC,
	cin => \inst4|SUMA[2]~37\,
	combout => \inst4|SUMA[3]~38_combout\,
	cout => \inst4|SUMA[3]~39\);

-- Location: FF_X12_Y11_N7
\inst4|SUMA[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[3]~38_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(3));

-- Location: LCCOMB_X12_Y11_N8
\inst4|SUMA[4]~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[4]~40_combout\ = (\inst4|SUMA\(4) & (\inst4|SUMA[3]~39\ $ (GND))) # (!\inst4|SUMA\(4) & (!\inst4|SUMA[3]~39\ & VCC))
-- \inst4|SUMA[4]~41\ = CARRY((\inst4|SUMA\(4) & !\inst4|SUMA[3]~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(4),
	datad => VCC,
	cin => \inst4|SUMA[3]~39\,
	combout => \inst4|SUMA[4]~40_combout\,
	cout => \inst4|SUMA[4]~41\);

-- Location: FF_X12_Y11_N9
\inst4|SUMA[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[4]~40_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(4));

-- Location: LCCOMB_X12_Y11_N10
\inst4|SUMA[5]~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[5]~42_combout\ = (\inst4|SUMA\(5) & (!\inst4|SUMA[4]~41\)) # (!\inst4|SUMA\(5) & ((\inst4|SUMA[4]~41\) # (GND)))
-- \inst4|SUMA[5]~43\ = CARRY((!\inst4|SUMA[4]~41\) # (!\inst4|SUMA\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(5),
	datad => VCC,
	cin => \inst4|SUMA[4]~41\,
	combout => \inst4|SUMA[5]~42_combout\,
	cout => \inst4|SUMA[5]~43\);

-- Location: FF_X12_Y11_N11
\inst4|SUMA[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[5]~42_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(5));

-- Location: LCCOMB_X12_Y11_N12
\inst4|SUMA[6]~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[6]~44_combout\ = (\inst4|SUMA\(6) & (\inst4|SUMA[5]~43\ $ (GND))) # (!\inst4|SUMA\(6) & (!\inst4|SUMA[5]~43\ & VCC))
-- \inst4|SUMA[6]~45\ = CARRY((\inst4|SUMA\(6) & !\inst4|SUMA[5]~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(6),
	datad => VCC,
	cin => \inst4|SUMA[5]~43\,
	combout => \inst4|SUMA[6]~44_combout\,
	cout => \inst4|SUMA[6]~45\);

-- Location: FF_X12_Y11_N13
\inst4|SUMA[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[6]~44_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(6));

-- Location: LCCOMB_X12_Y11_N14
\inst4|SUMA[7]~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[7]~46_combout\ = (\inst4|SUMA\(7) & (!\inst4|SUMA[6]~45\)) # (!\inst4|SUMA\(7) & ((\inst4|SUMA[6]~45\) # (GND)))
-- \inst4|SUMA[7]~47\ = CARRY((!\inst4|SUMA[6]~45\) # (!\inst4|SUMA\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(7),
	datad => VCC,
	cin => \inst4|SUMA[6]~45\,
	combout => \inst4|SUMA[7]~46_combout\,
	cout => \inst4|SUMA[7]~47\);

-- Location: FF_X12_Y11_N15
\inst4|SUMA[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[7]~46_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(7));

-- Location: LCCOMB_X12_Y11_N16
\inst4|SUMA[8]~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[8]~48_combout\ = (\inst4|SUMA\(8) & (\inst4|SUMA[7]~47\ $ (GND))) # (!\inst4|SUMA\(8) & (!\inst4|SUMA[7]~47\ & VCC))
-- \inst4|SUMA[8]~49\ = CARRY((\inst4|SUMA\(8) & !\inst4|SUMA[7]~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(8),
	datad => VCC,
	cin => \inst4|SUMA[7]~47\,
	combout => \inst4|SUMA[8]~48_combout\,
	cout => \inst4|SUMA[8]~49\);

-- Location: FF_X12_Y11_N17
\inst4|SUMA[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[8]~48_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(8));

-- Location: LCCOMB_X12_Y11_N18
\inst4|SUMA[9]~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[9]~50_combout\ = (\inst4|SUMA\(9) & (!\inst4|SUMA[8]~49\)) # (!\inst4|SUMA\(9) & ((\inst4|SUMA[8]~49\) # (GND)))
-- \inst4|SUMA[9]~51\ = CARRY((!\inst4|SUMA[8]~49\) # (!\inst4|SUMA\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(9),
	datad => VCC,
	cin => \inst4|SUMA[8]~49\,
	combout => \inst4|SUMA[9]~50_combout\,
	cout => \inst4|SUMA[9]~51\);

-- Location: FF_X12_Y11_N19
\inst4|SUMA[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[9]~50_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(9));

-- Location: LCCOMB_X12_Y11_N20
\inst4|SUMA[10]~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[10]~52_combout\ = (\inst4|SUMA\(10) & (\inst4|SUMA[9]~51\ $ (GND))) # (!\inst4|SUMA\(10) & (!\inst4|SUMA[9]~51\ & VCC))
-- \inst4|SUMA[10]~53\ = CARRY((\inst4|SUMA\(10) & !\inst4|SUMA[9]~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(10),
	datad => VCC,
	cin => \inst4|SUMA[9]~51\,
	combout => \inst4|SUMA[10]~52_combout\,
	cout => \inst4|SUMA[10]~53\);

-- Location: FF_X12_Y11_N21
\inst4|SUMA[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[10]~52_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(10));

-- Location: LCCOMB_X12_Y11_N22
\inst4|SUMA[11]~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[11]~54_combout\ = (\inst4|SUMA\(11) & (!\inst4|SUMA[10]~53\)) # (!\inst4|SUMA\(11) & ((\inst4|SUMA[10]~53\) # (GND)))
-- \inst4|SUMA[11]~55\ = CARRY((!\inst4|SUMA[10]~53\) # (!\inst4|SUMA\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(11),
	datad => VCC,
	cin => \inst4|SUMA[10]~53\,
	combout => \inst4|SUMA[11]~54_combout\,
	cout => \inst4|SUMA[11]~55\);

-- Location: FF_X12_Y11_N23
\inst4|SUMA[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[11]~54_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(11));

-- Location: LCCOMB_X12_Y11_N24
\inst4|SUMA[12]~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[12]~56_combout\ = (\inst4|SUMA\(12) & (\inst4|SUMA[11]~55\ $ (GND))) # (!\inst4|SUMA\(12) & (!\inst4|SUMA[11]~55\ & VCC))
-- \inst4|SUMA[12]~57\ = CARRY((\inst4|SUMA\(12) & !\inst4|SUMA[11]~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(12),
	datad => VCC,
	cin => \inst4|SUMA[11]~55\,
	combout => \inst4|SUMA[12]~56_combout\,
	cout => \inst4|SUMA[12]~57\);

-- Location: FF_X12_Y11_N25
\inst4|SUMA[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[12]~56_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(12));

-- Location: LCCOMB_X12_Y11_N26
\inst4|SUMA[13]~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[13]~58_combout\ = (\inst4|SUMA\(13) & (!\inst4|SUMA[12]~57\)) # (!\inst4|SUMA\(13) & ((\inst4|SUMA[12]~57\) # (GND)))
-- \inst4|SUMA[13]~59\ = CARRY((!\inst4|SUMA[12]~57\) # (!\inst4|SUMA\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(13),
	datad => VCC,
	cin => \inst4|SUMA[12]~57\,
	combout => \inst4|SUMA[13]~58_combout\,
	cout => \inst4|SUMA[13]~59\);

-- Location: FF_X12_Y11_N27
\inst4|SUMA[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[13]~58_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(13));

-- Location: LCCOMB_X12_Y11_N28
\inst4|SUMA[14]~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[14]~60_combout\ = (\inst4|SUMA\(14) & (\inst4|SUMA[13]~59\ $ (GND))) # (!\inst4|SUMA\(14) & (!\inst4|SUMA[13]~59\ & VCC))
-- \inst4|SUMA[14]~61\ = CARRY((\inst4|SUMA\(14) & !\inst4|SUMA[13]~59\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(14),
	datad => VCC,
	cin => \inst4|SUMA[13]~59\,
	combout => \inst4|SUMA[14]~60_combout\,
	cout => \inst4|SUMA[14]~61\);

-- Location: FF_X12_Y11_N29
\inst4|SUMA[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[14]~60_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(14));

-- Location: LCCOMB_X12_Y11_N30
\inst4|SUMA[15]~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[15]~62_combout\ = (\inst4|SUMA\(15) & (!\inst4|SUMA[14]~61\)) # (!\inst4|SUMA\(15) & ((\inst4|SUMA[14]~61\) # (GND)))
-- \inst4|SUMA[15]~63\ = CARRY((!\inst4|SUMA[14]~61\) # (!\inst4|SUMA\(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(15),
	datad => VCC,
	cin => \inst4|SUMA[14]~61\,
	combout => \inst4|SUMA[15]~62_combout\,
	cout => \inst4|SUMA[15]~63\);

-- Location: FF_X12_Y11_N31
\inst4|SUMA[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[15]~62_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(15));

-- Location: LCCOMB_X12_Y10_N0
\inst4|SUMA[16]~64\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[16]~64_combout\ = (\inst4|SUMA\(16) & (\inst4|SUMA[15]~63\ $ (GND))) # (!\inst4|SUMA\(16) & (!\inst4|SUMA[15]~63\ & VCC))
-- \inst4|SUMA[16]~65\ = CARRY((\inst4|SUMA\(16) & !\inst4|SUMA[15]~63\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(16),
	datad => VCC,
	cin => \inst4|SUMA[15]~63\,
	combout => \inst4|SUMA[16]~64_combout\,
	cout => \inst4|SUMA[16]~65\);

-- Location: FF_X12_Y10_N1
\inst4|SUMA[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[16]~64_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(16));

-- Location: LCCOMB_X12_Y10_N2
\inst4|SUMA[17]~66\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[17]~66_combout\ = (\inst4|SUMA\(17) & (!\inst4|SUMA[16]~65\)) # (!\inst4|SUMA\(17) & ((\inst4|SUMA[16]~65\) # (GND)))
-- \inst4|SUMA[17]~67\ = CARRY((!\inst4|SUMA[16]~65\) # (!\inst4|SUMA\(17)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(17),
	datad => VCC,
	cin => \inst4|SUMA[16]~65\,
	combout => \inst4|SUMA[17]~66_combout\,
	cout => \inst4|SUMA[17]~67\);

-- Location: FF_X12_Y10_N3
\inst4|SUMA[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[17]~66_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(17));

-- Location: LCCOMB_X12_Y10_N4
\inst4|SUMA[18]~68\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[18]~68_combout\ = (\inst4|SUMA\(18) & (\inst4|SUMA[17]~67\ $ (GND))) # (!\inst4|SUMA\(18) & (!\inst4|SUMA[17]~67\ & VCC))
-- \inst4|SUMA[18]~69\ = CARRY((\inst4|SUMA\(18) & !\inst4|SUMA[17]~67\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(18),
	datad => VCC,
	cin => \inst4|SUMA[17]~67\,
	combout => \inst4|SUMA[18]~68_combout\,
	cout => \inst4|SUMA[18]~69\);

-- Location: FF_X12_Y10_N5
\inst4|SUMA[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[18]~68_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(18));

-- Location: LCCOMB_X12_Y10_N6
\inst4|SUMA[19]~70\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[19]~70_combout\ = (\inst4|SUMA\(19) & (!\inst4|SUMA[18]~69\)) # (!\inst4|SUMA\(19) & ((\inst4|SUMA[18]~69\) # (GND)))
-- \inst4|SUMA[19]~71\ = CARRY((!\inst4|SUMA[18]~69\) # (!\inst4|SUMA\(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(19),
	datad => VCC,
	cin => \inst4|SUMA[18]~69\,
	combout => \inst4|SUMA[19]~70_combout\,
	cout => \inst4|SUMA[19]~71\);

-- Location: FF_X12_Y10_N7
\inst4|SUMA[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[19]~70_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(19));

-- Location: LCCOMB_X12_Y10_N8
\inst4|SUMA[20]~72\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[20]~72_combout\ = (\inst4|SUMA\(20) & (\inst4|SUMA[19]~71\ $ (GND))) # (!\inst4|SUMA\(20) & (!\inst4|SUMA[19]~71\ & VCC))
-- \inst4|SUMA[20]~73\ = CARRY((\inst4|SUMA\(20) & !\inst4|SUMA[19]~71\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(20),
	datad => VCC,
	cin => \inst4|SUMA[19]~71\,
	combout => \inst4|SUMA[20]~72_combout\,
	cout => \inst4|SUMA[20]~73\);

-- Location: FF_X12_Y10_N9
\inst4|SUMA[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[20]~72_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(20));

-- Location: LCCOMB_X12_Y10_N10
\inst4|SUMA[21]~74\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[21]~74_combout\ = (\inst4|SUMA\(21) & (!\inst4|SUMA[20]~73\)) # (!\inst4|SUMA\(21) & ((\inst4|SUMA[20]~73\) # (GND)))
-- \inst4|SUMA[21]~75\ = CARRY((!\inst4|SUMA[20]~73\) # (!\inst4|SUMA\(21)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(21),
	datad => VCC,
	cin => \inst4|SUMA[20]~73\,
	combout => \inst4|SUMA[21]~74_combout\,
	cout => \inst4|SUMA[21]~75\);

-- Location: FF_X12_Y10_N11
\inst4|SUMA[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[21]~74_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(21));

-- Location: LCCOMB_X12_Y10_N12
\inst4|SUMA[22]~76\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[22]~76_combout\ = (\inst4|SUMA\(22) & (\inst4|SUMA[21]~75\ $ (GND))) # (!\inst4|SUMA\(22) & (!\inst4|SUMA[21]~75\ & VCC))
-- \inst4|SUMA[22]~77\ = CARRY((\inst4|SUMA\(22) & !\inst4|SUMA[21]~75\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(22),
	datad => VCC,
	cin => \inst4|SUMA[21]~75\,
	combout => \inst4|SUMA[22]~76_combout\,
	cout => \inst4|SUMA[22]~77\);

-- Location: FF_X12_Y10_N13
\inst4|SUMA[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[22]~76_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(22));

-- Location: LCCOMB_X12_Y10_N14
\inst4|SUMA[23]~78\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[23]~78_combout\ = (\inst4|SUMA\(23) & (!\inst4|SUMA[22]~77\)) # (!\inst4|SUMA\(23) & ((\inst4|SUMA[22]~77\) # (GND)))
-- \inst4|SUMA[23]~79\ = CARRY((!\inst4|SUMA[22]~77\) # (!\inst4|SUMA\(23)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(23),
	datad => VCC,
	cin => \inst4|SUMA[22]~77\,
	combout => \inst4|SUMA[23]~78_combout\,
	cout => \inst4|SUMA[23]~79\);

-- Location: FF_X12_Y10_N15
\inst4|SUMA[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[23]~78_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(23));

-- Location: LCCOMB_X12_Y10_N16
\inst4|SUMA[24]~80\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[24]~80_combout\ = (\inst4|SUMA\(24) & (\inst4|SUMA[23]~79\ $ (GND))) # (!\inst4|SUMA\(24) & (!\inst4|SUMA[23]~79\ & VCC))
-- \inst4|SUMA[24]~81\ = CARRY((\inst4|SUMA\(24) & !\inst4|SUMA[23]~79\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(24),
	datad => VCC,
	cin => \inst4|SUMA[23]~79\,
	combout => \inst4|SUMA[24]~80_combout\,
	cout => \inst4|SUMA[24]~81\);

-- Location: FF_X12_Y10_N17
\inst4|SUMA[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[24]~80_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(24));

-- Location: LCCOMB_X12_Y10_N18
\inst4|SUMA[25]~82\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[25]~82_combout\ = (\inst4|SUMA\(25) & (!\inst4|SUMA[24]~81\)) # (!\inst4|SUMA\(25) & ((\inst4|SUMA[24]~81\) # (GND)))
-- \inst4|SUMA[25]~83\ = CARRY((!\inst4|SUMA[24]~81\) # (!\inst4|SUMA\(25)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(25),
	datad => VCC,
	cin => \inst4|SUMA[24]~81\,
	combout => \inst4|SUMA[25]~82_combout\,
	cout => \inst4|SUMA[25]~83\);

-- Location: FF_X12_Y10_N19
\inst4|SUMA[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[25]~82_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(25));

-- Location: LCCOMB_X12_Y10_N20
\inst4|SUMA[26]~84\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[26]~84_combout\ = (\inst4|SUMA\(26) & (\inst4|SUMA[25]~83\ $ (GND))) # (!\inst4|SUMA\(26) & (!\inst4|SUMA[25]~83\ & VCC))
-- \inst4|SUMA[26]~85\ = CARRY((\inst4|SUMA\(26) & !\inst4|SUMA[25]~83\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(26),
	datad => VCC,
	cin => \inst4|SUMA[25]~83\,
	combout => \inst4|SUMA[26]~84_combout\,
	cout => \inst4|SUMA[26]~85\);

-- Location: FF_X12_Y10_N21
\inst4|SUMA[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[26]~84_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(26));

-- Location: LCCOMB_X12_Y10_N22
\inst4|SUMA[27]~86\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[27]~86_combout\ = (\inst4|SUMA\(27) & (!\inst4|SUMA[26]~85\)) # (!\inst4|SUMA\(27) & ((\inst4|SUMA[26]~85\) # (GND)))
-- \inst4|SUMA[27]~87\ = CARRY((!\inst4|SUMA[26]~85\) # (!\inst4|SUMA\(27)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(27),
	datad => VCC,
	cin => \inst4|SUMA[26]~85\,
	combout => \inst4|SUMA[27]~86_combout\,
	cout => \inst4|SUMA[27]~87\);

-- Location: FF_X12_Y10_N23
\inst4|SUMA[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[27]~86_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(27));

-- Location: LCCOMB_X12_Y10_N24
\inst4|SUMA[28]~88\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[28]~88_combout\ = (\inst4|SUMA\(28) & (\inst4|SUMA[27]~87\ $ (GND))) # (!\inst4|SUMA\(28) & (!\inst4|SUMA[27]~87\ & VCC))
-- \inst4|SUMA[28]~89\ = CARRY((\inst4|SUMA\(28) & !\inst4|SUMA[27]~87\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(28),
	datad => VCC,
	cin => \inst4|SUMA[27]~87\,
	combout => \inst4|SUMA[28]~88_combout\,
	cout => \inst4|SUMA[28]~89\);

-- Location: FF_X12_Y10_N25
\inst4|SUMA[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[28]~88_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(28));

-- Location: LCCOMB_X12_Y10_N26
\inst4|SUMA[29]~90\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[29]~90_combout\ = (\inst4|SUMA\(29) & (!\inst4|SUMA[28]~89\)) # (!\inst4|SUMA\(29) & ((\inst4|SUMA[28]~89\) # (GND)))
-- \inst4|SUMA[29]~91\ = CARRY((!\inst4|SUMA[28]~89\) # (!\inst4|SUMA\(29)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(29),
	datad => VCC,
	cin => \inst4|SUMA[28]~89\,
	combout => \inst4|SUMA[29]~90_combout\,
	cout => \inst4|SUMA[29]~91\);

-- Location: FF_X12_Y10_N27
\inst4|SUMA[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[29]~90_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(29));

-- Location: LCCOMB_X12_Y10_N28
\inst4|SUMA[30]~92\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[30]~92_combout\ = (\inst4|SUMA\(30) & (\inst4|SUMA[29]~91\ $ (GND))) # (!\inst4|SUMA\(30) & (!\inst4|SUMA[29]~91\ & VCC))
-- \inst4|SUMA[30]~93\ = CARRY((\inst4|SUMA\(30) & !\inst4|SUMA[29]~91\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \inst4|SUMA\(30),
	datad => VCC,
	cin => \inst4|SUMA[29]~91\,
	combout => \inst4|SUMA[30]~92_combout\,
	cout => \inst4|SUMA[30]~93\);

-- Location: FF_X12_Y10_N29
\inst4|SUMA[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[30]~92_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(30));

-- Location: LCCOMB_X12_Y10_N30
\inst4|SUMA[31]~94\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMA[31]~94_combout\ = \inst4|SUMA\(31) $ (\inst4|SUMA[30]~93\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(31),
	cin => \inst4|SUMA[30]~93\,
	combout => \inst4|SUMA[31]~94_combout\);

-- Location: FF_X12_Y10_N31
\inst4|SUMA[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \RF~inputclkctrl_outclk\,
	d => \inst4|SUMA[31]~94_combout\,
	ena => \inst4|SUMA~106_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMA\(31));

-- Location: LCCOMB_X11_Y10_N16
\inst4|SUMB[31]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[31]~feeder_combout\ = \inst4|SUMA\(31)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst4|SUMA\(31),
	combout => \inst4|SUMB[31]~feeder_combout\);

-- Location: FF_X11_Y10_N17
\inst4|SUMB[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[31]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(31));

-- Location: LCCOMB_X11_Y10_N10
\inst4|SUMB[30]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[30]~feeder_combout\ = \inst4|SUMA\(30)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(30),
	combout => \inst4|SUMB[30]~feeder_combout\);

-- Location: FF_X11_Y10_N11
\inst4|SUMB[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[30]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(30));

-- Location: LCCOMB_X11_Y10_N12
\inst4|SUMB[29]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[29]~feeder_combout\ = \inst4|SUMA\(29)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(29),
	combout => \inst4|SUMB[29]~feeder_combout\);

-- Location: FF_X11_Y10_N13
\inst4|SUMB[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[29]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(29));

-- Location: LCCOMB_X11_Y10_N30
\inst4|SUMB[28]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[28]~feeder_combout\ = \inst4|SUMA\(28)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(28),
	combout => \inst4|SUMB[28]~feeder_combout\);

-- Location: FF_X11_Y10_N31
\inst4|SUMB[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[28]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(28));

-- Location: LCCOMB_X11_Y10_N4
\inst4|SUMB[27]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[27]~feeder_combout\ = \inst4|SUMA\(27)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(27),
	combout => \inst4|SUMB[27]~feeder_combout\);

-- Location: FF_X11_Y10_N5
\inst4|SUMB[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[27]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(27));

-- Location: LCCOMB_X11_Y10_N26
\inst4|SUMB[26]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[26]~feeder_combout\ = \inst4|SUMA\(26)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst4|SUMA\(26),
	combout => \inst4|SUMB[26]~feeder_combout\);

-- Location: FF_X11_Y10_N27
\inst4|SUMB[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[26]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(26));

-- Location: LCCOMB_X11_Y10_N28
\inst4|SUMB[25]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[25]~feeder_combout\ = \inst4|SUMA\(25)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(25),
	combout => \inst4|SUMB[25]~feeder_combout\);

-- Location: FF_X11_Y10_N29
\inst4|SUMB[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[25]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(25));

-- Location: LCCOMB_X11_Y10_N14
\inst4|SUMB[24]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[24]~feeder_combout\ = \inst4|SUMA\(24)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(24),
	combout => \inst4|SUMB[24]~feeder_combout\);

-- Location: FF_X11_Y10_N15
\inst4|SUMB[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[24]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(24));

-- Location: LCCOMB_X11_Y10_N20
\inst4|SUMB[23]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[23]~feeder_combout\ = \inst4|SUMA\(23)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst4|SUMA\(23),
	combout => \inst4|SUMB[23]~feeder_combout\);

-- Location: FF_X11_Y10_N21
\inst4|SUMB[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[23]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(23));

-- Location: LCCOMB_X11_Y10_N18
\inst4|SUMB[22]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[22]~feeder_combout\ = \inst4|SUMA\(22)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst4|SUMA\(22),
	combout => \inst4|SUMB[22]~feeder_combout\);

-- Location: FF_X11_Y10_N19
\inst4|SUMB[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[22]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(22));

-- Location: LCCOMB_X11_Y10_N8
\inst4|SUMB[21]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[21]~feeder_combout\ = \inst4|SUMA\(21)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst4|SUMA\(21),
	combout => \inst4|SUMB[21]~feeder_combout\);

-- Location: FF_X11_Y10_N9
\inst4|SUMB[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[21]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(21));

-- Location: LCCOMB_X11_Y10_N6
\inst4|SUMB[20]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[20]~feeder_combout\ = \inst4|SUMA\(20)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst4|SUMA\(20),
	combout => \inst4|SUMB[20]~feeder_combout\);

-- Location: FF_X11_Y10_N7
\inst4|SUMB[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[20]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(20));

-- Location: LCCOMB_X11_Y10_N24
\inst4|SUMB[19]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[19]~feeder_combout\ = \inst4|SUMA\(19)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(19),
	combout => \inst4|SUMB[19]~feeder_combout\);

-- Location: FF_X11_Y10_N25
\inst4|SUMB[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[19]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(19));

-- Location: LCCOMB_X14_Y10_N20
\inst4|SUMB[18]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[18]~feeder_combout\ = \inst4|SUMA\(18)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(18),
	combout => \inst4|SUMB[18]~feeder_combout\);

-- Location: FF_X14_Y10_N21
\inst4|SUMB[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[18]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(18));

-- Location: LCCOMB_X14_Y10_N26
\inst4|SUMB[17]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[17]~feeder_combout\ = \inst4|SUMA\(17)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst4|SUMA\(17),
	combout => \inst4|SUMB[17]~feeder_combout\);

-- Location: FF_X14_Y10_N27
\inst4|SUMB[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[17]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(17));

-- Location: LCCOMB_X14_Y10_N0
\inst4|SUMB[16]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[16]~feeder_combout\ = \inst4|SUMA\(16)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(16),
	combout => \inst4|SUMB[16]~feeder_combout\);

-- Location: FF_X14_Y10_N1
\inst4|SUMB[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[16]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(16));

-- Location: LCCOMB_X14_Y11_N24
\inst4|SUMB[15]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[15]~feeder_combout\ = \inst4|SUMA\(15)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(15),
	combout => \inst4|SUMB[15]~feeder_combout\);

-- Location: FF_X14_Y11_N25
\inst4|SUMB[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[15]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(15));

-- Location: LCCOMB_X14_Y11_N6
\inst4|SUMB[14]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[14]~feeder_combout\ = \inst4|SUMA\(14)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(14),
	combout => \inst4|SUMB[14]~feeder_combout\);

-- Location: FF_X14_Y11_N7
\inst4|SUMB[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[14]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(14));

-- Location: LCCOMB_X14_Y11_N16
\inst4|SUMB[13]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[13]~feeder_combout\ = \inst4|SUMA\(13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst4|SUMA\(13),
	combout => \inst4|SUMB[13]~feeder_combout\);

-- Location: FF_X14_Y11_N17
\inst4|SUMB[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[13]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(13));

-- Location: LCCOMB_X14_Y11_N14
\inst4|SUMB[12]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[12]~feeder_combout\ = \inst4|SUMA\(12)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(12),
	combout => \inst4|SUMB[12]~feeder_combout\);

-- Location: FF_X14_Y11_N15
\inst4|SUMB[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[12]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(12));

-- Location: LCCOMB_X14_Y11_N4
\inst4|SUMB[11]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[11]~feeder_combout\ = \inst4|SUMA\(11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(11),
	combout => \inst4|SUMB[11]~feeder_combout\);

-- Location: FF_X14_Y11_N5
\inst4|SUMB[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[11]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(11));

-- Location: LCCOMB_X14_Y11_N30
\inst4|SUMB[10]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[10]~feeder_combout\ = \inst4|SUMA\(10)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(10),
	combout => \inst4|SUMB[10]~feeder_combout\);

-- Location: FF_X14_Y11_N31
\inst4|SUMB[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[10]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(10));

-- Location: LCCOMB_X14_Y11_N28
\inst4|SUMB[9]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[9]~feeder_combout\ = \inst4|SUMA\(9)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst4|SUMA\(9),
	combout => \inst4|SUMB[9]~feeder_combout\);

-- Location: FF_X14_Y11_N29
\inst4|SUMB[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[9]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(9));

-- Location: LCCOMB_X14_Y11_N22
\inst4|SUMB[8]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[8]~feeder_combout\ = \inst4|SUMA\(8)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(8),
	combout => \inst4|SUMB[8]~feeder_combout\);

-- Location: FF_X14_Y11_N23
\inst4|SUMB[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[8]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(8));

-- Location: LCCOMB_X14_Y11_N8
\inst4|SUMB[7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[7]~feeder_combout\ = \inst4|SUMA\(7)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(7),
	combout => \inst4|SUMB[7]~feeder_combout\);

-- Location: FF_X14_Y11_N9
\inst4|SUMB[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[7]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(7));

-- Location: LCCOMB_X14_Y11_N26
\inst4|SUMB[6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[6]~feeder_combout\ = \inst4|SUMA\(6)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(6),
	combout => \inst4|SUMB[6]~feeder_combout\);

-- Location: FF_X14_Y11_N27
\inst4|SUMB[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[6]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(6));

-- Location: LCCOMB_X14_Y11_N0
\inst4|SUMB[5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[5]~feeder_combout\ = \inst4|SUMA\(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst4|SUMA\(5),
	combout => \inst4|SUMB[5]~feeder_combout\);

-- Location: FF_X14_Y11_N1
\inst4|SUMB[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[5]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(5));

-- Location: LCCOMB_X14_Y11_N10
\inst4|SUMB[4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[4]~feeder_combout\ = \inst4|SUMA\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(4),
	combout => \inst4|SUMB[4]~feeder_combout\);

-- Location: FF_X14_Y11_N11
\inst4|SUMB[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[4]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(4));

-- Location: LCCOMB_X14_Y11_N20
\inst4|SUMB[3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[3]~feeder_combout\ = \inst4|SUMA\(3)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst4|SUMA\(3),
	combout => \inst4|SUMB[3]~feeder_combout\);

-- Location: FF_X14_Y11_N21
\inst4|SUMB[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[3]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(3));

-- Location: LCCOMB_X14_Y11_N2
\inst4|SUMB[2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[2]~feeder_combout\ = \inst4|SUMA\(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \inst4|SUMA\(2),
	combout => \inst4|SUMB[2]~feeder_combout\);

-- Location: FF_X14_Y11_N3
\inst4|SUMB[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[2]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(2));

-- Location: LCCOMB_X14_Y11_N12
\inst4|SUMB[1]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[1]~feeder_combout\ = \inst4|SUMA\(1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(1),
	combout => \inst4|SUMB[1]~feeder_combout\);

-- Location: FF_X14_Y11_N13
\inst4|SUMB[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[1]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(1));

-- Location: LCCOMB_X14_Y11_N18
\inst4|SUMB[0]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|SUMB[0]~feeder_combout\ = \inst4|SUMA\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \inst4|SUMA\(0),
	combout => \inst4|SUMB[0]~feeder_combout\);

-- Location: FF_X14_Y11_N19
\inst4|SUMB[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|SUMB[0]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|SUMB\(0));

-- Location: LCCOMB_X13_Y11_N0
\inst4|ADD[0]~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[0]~32_combout\ = (\inst4|SUMA\(0) & ((GND) # (!\inst4|SUMB\(0)))) # (!\inst4|SUMA\(0) & (\inst4|SUMB\(0) $ (GND)))
-- \inst4|ADD[0]~33\ = CARRY((\inst4|SUMA\(0)) # (!\inst4|SUMB\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(0),
	datab => \inst4|SUMB\(0),
	datad => VCC,
	combout => \inst4|ADD[0]~32_combout\,
	cout => \inst4|ADD[0]~33\);

-- Location: LCCOMB_X13_Y11_N2
\inst4|ADD[1]~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[1]~34_combout\ = (\inst4|SUMA\(1) & ((\inst4|SUMB\(1) & (!\inst4|ADD[0]~33\)) # (!\inst4|SUMB\(1) & (\inst4|ADD[0]~33\ & VCC)))) # (!\inst4|SUMA\(1) & ((\inst4|SUMB\(1) & ((\inst4|ADD[0]~33\) # (GND))) # (!\inst4|SUMB\(1) & 
-- (!\inst4|ADD[0]~33\))))
-- \inst4|ADD[1]~35\ = CARRY((\inst4|SUMA\(1) & (\inst4|SUMB\(1) & !\inst4|ADD[0]~33\)) # (!\inst4|SUMA\(1) & ((\inst4|SUMB\(1)) # (!\inst4|ADD[0]~33\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(1),
	datab => \inst4|SUMB\(1),
	datad => VCC,
	cin => \inst4|ADD[0]~33\,
	combout => \inst4|ADD[1]~34_combout\,
	cout => \inst4|ADD[1]~35\);

-- Location: LCCOMB_X13_Y11_N4
\inst4|ADD[2]~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[2]~36_combout\ = ((\inst4|SUMB\(2) $ (\inst4|SUMA\(2) $ (\inst4|ADD[1]~35\)))) # (GND)
-- \inst4|ADD[2]~37\ = CARRY((\inst4|SUMB\(2) & (\inst4|SUMA\(2) & !\inst4|ADD[1]~35\)) # (!\inst4|SUMB\(2) & ((\inst4|SUMA\(2)) # (!\inst4|ADD[1]~35\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMB\(2),
	datab => \inst4|SUMA\(2),
	datad => VCC,
	cin => \inst4|ADD[1]~35\,
	combout => \inst4|ADD[2]~36_combout\,
	cout => \inst4|ADD[2]~37\);

-- Location: LCCOMB_X13_Y11_N6
\inst4|ADD[3]~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[3]~38_combout\ = (\inst4|SUMB\(3) & ((\inst4|SUMA\(3) & (!\inst4|ADD[2]~37\)) # (!\inst4|SUMA\(3) & ((\inst4|ADD[2]~37\) # (GND))))) # (!\inst4|SUMB\(3) & ((\inst4|SUMA\(3) & (\inst4|ADD[2]~37\ & VCC)) # (!\inst4|SUMA\(3) & 
-- (!\inst4|ADD[2]~37\))))
-- \inst4|ADD[3]~39\ = CARRY((\inst4|SUMB\(3) & ((!\inst4|ADD[2]~37\) # (!\inst4|SUMA\(3)))) # (!\inst4|SUMB\(3) & (!\inst4|SUMA\(3) & !\inst4|ADD[2]~37\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMB\(3),
	datab => \inst4|SUMA\(3),
	datad => VCC,
	cin => \inst4|ADD[2]~37\,
	combout => \inst4|ADD[3]~38_combout\,
	cout => \inst4|ADD[3]~39\);

-- Location: LCCOMB_X13_Y11_N8
\inst4|ADD[4]~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[4]~40_combout\ = ((\inst4|SUMA\(4) $ (\inst4|SUMB\(4) $ (\inst4|ADD[3]~39\)))) # (GND)
-- \inst4|ADD[4]~41\ = CARRY((\inst4|SUMA\(4) & ((!\inst4|ADD[3]~39\) # (!\inst4|SUMB\(4)))) # (!\inst4|SUMA\(4) & (!\inst4|SUMB\(4) & !\inst4|ADD[3]~39\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(4),
	datab => \inst4|SUMB\(4),
	datad => VCC,
	cin => \inst4|ADD[3]~39\,
	combout => \inst4|ADD[4]~40_combout\,
	cout => \inst4|ADD[4]~41\);

-- Location: LCCOMB_X13_Y11_N10
\inst4|ADD[5]~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[5]~42_combout\ = (\inst4|SUMA\(5) & ((\inst4|SUMB\(5) & (!\inst4|ADD[4]~41\)) # (!\inst4|SUMB\(5) & (\inst4|ADD[4]~41\ & VCC)))) # (!\inst4|SUMA\(5) & ((\inst4|SUMB\(5) & ((\inst4|ADD[4]~41\) # (GND))) # (!\inst4|SUMB\(5) & 
-- (!\inst4|ADD[4]~41\))))
-- \inst4|ADD[5]~43\ = CARRY((\inst4|SUMA\(5) & (\inst4|SUMB\(5) & !\inst4|ADD[4]~41\)) # (!\inst4|SUMA\(5) & ((\inst4|SUMB\(5)) # (!\inst4|ADD[4]~41\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(5),
	datab => \inst4|SUMB\(5),
	datad => VCC,
	cin => \inst4|ADD[4]~41\,
	combout => \inst4|ADD[5]~42_combout\,
	cout => \inst4|ADD[5]~43\);

-- Location: LCCOMB_X13_Y11_N12
\inst4|ADD[6]~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[6]~44_combout\ = ((\inst4|SUMA\(6) $ (\inst4|SUMB\(6) $ (\inst4|ADD[5]~43\)))) # (GND)
-- \inst4|ADD[6]~45\ = CARRY((\inst4|SUMA\(6) & ((!\inst4|ADD[5]~43\) # (!\inst4|SUMB\(6)))) # (!\inst4|SUMA\(6) & (!\inst4|SUMB\(6) & !\inst4|ADD[5]~43\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(6),
	datab => \inst4|SUMB\(6),
	datad => VCC,
	cin => \inst4|ADD[5]~43\,
	combout => \inst4|ADD[6]~44_combout\,
	cout => \inst4|ADD[6]~45\);

-- Location: LCCOMB_X13_Y11_N14
\inst4|ADD[7]~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[7]~46_combout\ = (\inst4|SUMA\(7) & ((\inst4|SUMB\(7) & (!\inst4|ADD[6]~45\)) # (!\inst4|SUMB\(7) & (\inst4|ADD[6]~45\ & VCC)))) # (!\inst4|SUMA\(7) & ((\inst4|SUMB\(7) & ((\inst4|ADD[6]~45\) # (GND))) # (!\inst4|SUMB\(7) & 
-- (!\inst4|ADD[6]~45\))))
-- \inst4|ADD[7]~47\ = CARRY((\inst4|SUMA\(7) & (\inst4|SUMB\(7) & !\inst4|ADD[6]~45\)) # (!\inst4|SUMA\(7) & ((\inst4|SUMB\(7)) # (!\inst4|ADD[6]~45\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(7),
	datab => \inst4|SUMB\(7),
	datad => VCC,
	cin => \inst4|ADD[6]~45\,
	combout => \inst4|ADD[7]~46_combout\,
	cout => \inst4|ADD[7]~47\);

-- Location: LCCOMB_X13_Y11_N16
\inst4|ADD[8]~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[8]~48_combout\ = ((\inst4|SUMA\(8) $ (\inst4|SUMB\(8) $ (\inst4|ADD[7]~47\)))) # (GND)
-- \inst4|ADD[8]~49\ = CARRY((\inst4|SUMA\(8) & ((!\inst4|ADD[7]~47\) # (!\inst4|SUMB\(8)))) # (!\inst4|SUMA\(8) & (!\inst4|SUMB\(8) & !\inst4|ADD[7]~47\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(8),
	datab => \inst4|SUMB\(8),
	datad => VCC,
	cin => \inst4|ADD[7]~47\,
	combout => \inst4|ADD[8]~48_combout\,
	cout => \inst4|ADD[8]~49\);

-- Location: LCCOMB_X13_Y11_N18
\inst4|ADD[9]~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[9]~50_combout\ = (\inst4|SUMA\(9) & ((\inst4|SUMB\(9) & (!\inst4|ADD[8]~49\)) # (!\inst4|SUMB\(9) & (\inst4|ADD[8]~49\ & VCC)))) # (!\inst4|SUMA\(9) & ((\inst4|SUMB\(9) & ((\inst4|ADD[8]~49\) # (GND))) # (!\inst4|SUMB\(9) & 
-- (!\inst4|ADD[8]~49\))))
-- \inst4|ADD[9]~51\ = CARRY((\inst4|SUMA\(9) & (\inst4|SUMB\(9) & !\inst4|ADD[8]~49\)) # (!\inst4|SUMA\(9) & ((\inst4|SUMB\(9)) # (!\inst4|ADD[8]~49\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(9),
	datab => \inst4|SUMB\(9),
	datad => VCC,
	cin => \inst4|ADD[8]~49\,
	combout => \inst4|ADD[9]~50_combout\,
	cout => \inst4|ADD[9]~51\);

-- Location: LCCOMB_X13_Y11_N20
\inst4|ADD[10]~52\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[10]~52_combout\ = ((\inst4|SUMB\(10) $ (\inst4|SUMA\(10) $ (\inst4|ADD[9]~51\)))) # (GND)
-- \inst4|ADD[10]~53\ = CARRY((\inst4|SUMB\(10) & (\inst4|SUMA\(10) & !\inst4|ADD[9]~51\)) # (!\inst4|SUMB\(10) & ((\inst4|SUMA\(10)) # (!\inst4|ADD[9]~51\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMB\(10),
	datab => \inst4|SUMA\(10),
	datad => VCC,
	cin => \inst4|ADD[9]~51\,
	combout => \inst4|ADD[10]~52_combout\,
	cout => \inst4|ADD[10]~53\);

-- Location: LCCOMB_X13_Y11_N22
\inst4|ADD[11]~54\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[11]~54_combout\ = (\inst4|SUMB\(11) & ((\inst4|SUMA\(11) & (!\inst4|ADD[10]~53\)) # (!\inst4|SUMA\(11) & ((\inst4|ADD[10]~53\) # (GND))))) # (!\inst4|SUMB\(11) & ((\inst4|SUMA\(11) & (\inst4|ADD[10]~53\ & VCC)) # (!\inst4|SUMA\(11) & 
-- (!\inst4|ADD[10]~53\))))
-- \inst4|ADD[11]~55\ = CARRY((\inst4|SUMB\(11) & ((!\inst4|ADD[10]~53\) # (!\inst4|SUMA\(11)))) # (!\inst4|SUMB\(11) & (!\inst4|SUMA\(11) & !\inst4|ADD[10]~53\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMB\(11),
	datab => \inst4|SUMA\(11),
	datad => VCC,
	cin => \inst4|ADD[10]~53\,
	combout => \inst4|ADD[11]~54_combout\,
	cout => \inst4|ADD[11]~55\);

-- Location: LCCOMB_X13_Y11_N24
\inst4|ADD[12]~56\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[12]~56_combout\ = ((\inst4|SUMB\(12) $ (\inst4|SUMA\(12) $ (\inst4|ADD[11]~55\)))) # (GND)
-- \inst4|ADD[12]~57\ = CARRY((\inst4|SUMB\(12) & (\inst4|SUMA\(12) & !\inst4|ADD[11]~55\)) # (!\inst4|SUMB\(12) & ((\inst4|SUMA\(12)) # (!\inst4|ADD[11]~55\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMB\(12),
	datab => \inst4|SUMA\(12),
	datad => VCC,
	cin => \inst4|ADD[11]~55\,
	combout => \inst4|ADD[12]~56_combout\,
	cout => \inst4|ADD[12]~57\);

-- Location: LCCOMB_X13_Y11_N26
\inst4|ADD[13]~58\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[13]~58_combout\ = (\inst4|SUMA\(13) & ((\inst4|SUMB\(13) & (!\inst4|ADD[12]~57\)) # (!\inst4|SUMB\(13) & (\inst4|ADD[12]~57\ & VCC)))) # (!\inst4|SUMA\(13) & ((\inst4|SUMB\(13) & ((\inst4|ADD[12]~57\) # (GND))) # (!\inst4|SUMB\(13) & 
-- (!\inst4|ADD[12]~57\))))
-- \inst4|ADD[13]~59\ = CARRY((\inst4|SUMA\(13) & (\inst4|SUMB\(13) & !\inst4|ADD[12]~57\)) # (!\inst4|SUMA\(13) & ((\inst4|SUMB\(13)) # (!\inst4|ADD[12]~57\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(13),
	datab => \inst4|SUMB\(13),
	datad => VCC,
	cin => \inst4|ADD[12]~57\,
	combout => \inst4|ADD[13]~58_combout\,
	cout => \inst4|ADD[13]~59\);

-- Location: LCCOMB_X13_Y11_N28
\inst4|ADD[14]~60\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[14]~60_combout\ = ((\inst4|SUMB\(14) $ (\inst4|SUMA\(14) $ (\inst4|ADD[13]~59\)))) # (GND)
-- \inst4|ADD[14]~61\ = CARRY((\inst4|SUMB\(14) & (\inst4|SUMA\(14) & !\inst4|ADD[13]~59\)) # (!\inst4|SUMB\(14) & ((\inst4|SUMA\(14)) # (!\inst4|ADD[13]~59\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMB\(14),
	datab => \inst4|SUMA\(14),
	datad => VCC,
	cin => \inst4|ADD[13]~59\,
	combout => \inst4|ADD[14]~60_combout\,
	cout => \inst4|ADD[14]~61\);

-- Location: LCCOMB_X13_Y11_N30
\inst4|ADD[15]~62\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[15]~62_combout\ = (\inst4|SUMB\(15) & ((\inst4|SUMA\(15) & (!\inst4|ADD[14]~61\)) # (!\inst4|SUMA\(15) & ((\inst4|ADD[14]~61\) # (GND))))) # (!\inst4|SUMB\(15) & ((\inst4|SUMA\(15) & (\inst4|ADD[14]~61\ & VCC)) # (!\inst4|SUMA\(15) & 
-- (!\inst4|ADD[14]~61\))))
-- \inst4|ADD[15]~63\ = CARRY((\inst4|SUMB\(15) & ((!\inst4|ADD[14]~61\) # (!\inst4|SUMA\(15)))) # (!\inst4|SUMB\(15) & (!\inst4|SUMA\(15) & !\inst4|ADD[14]~61\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMB\(15),
	datab => \inst4|SUMA\(15),
	datad => VCC,
	cin => \inst4|ADD[14]~61\,
	combout => \inst4|ADD[15]~62_combout\,
	cout => \inst4|ADD[15]~63\);

-- Location: LCCOMB_X13_Y10_N0
\inst4|ADD[16]~64\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[16]~64_combout\ = ((\inst4|SUMA\(16) $ (\inst4|SUMB\(16) $ (\inst4|ADD[15]~63\)))) # (GND)
-- \inst4|ADD[16]~65\ = CARRY((\inst4|SUMA\(16) & ((!\inst4|ADD[15]~63\) # (!\inst4|SUMB\(16)))) # (!\inst4|SUMA\(16) & (!\inst4|SUMB\(16) & !\inst4|ADD[15]~63\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(16),
	datab => \inst4|SUMB\(16),
	datad => VCC,
	cin => \inst4|ADD[15]~63\,
	combout => \inst4|ADD[16]~64_combout\,
	cout => \inst4|ADD[16]~65\);

-- Location: LCCOMB_X13_Y10_N2
\inst4|ADD[17]~66\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[17]~66_combout\ = (\inst4|SUMA\(17) & ((\inst4|SUMB\(17) & (!\inst4|ADD[16]~65\)) # (!\inst4|SUMB\(17) & (\inst4|ADD[16]~65\ & VCC)))) # (!\inst4|SUMA\(17) & ((\inst4|SUMB\(17) & ((\inst4|ADD[16]~65\) # (GND))) # (!\inst4|SUMB\(17) & 
-- (!\inst4|ADD[16]~65\))))
-- \inst4|ADD[17]~67\ = CARRY((\inst4|SUMA\(17) & (\inst4|SUMB\(17) & !\inst4|ADD[16]~65\)) # (!\inst4|SUMA\(17) & ((\inst4|SUMB\(17)) # (!\inst4|ADD[16]~65\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(17),
	datab => \inst4|SUMB\(17),
	datad => VCC,
	cin => \inst4|ADD[16]~65\,
	combout => \inst4|ADD[17]~66_combout\,
	cout => \inst4|ADD[17]~67\);

-- Location: LCCOMB_X13_Y10_N4
\inst4|ADD[18]~68\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[18]~68_combout\ = ((\inst4|SUMB\(18) $ (\inst4|SUMA\(18) $ (\inst4|ADD[17]~67\)))) # (GND)
-- \inst4|ADD[18]~69\ = CARRY((\inst4|SUMB\(18) & (\inst4|SUMA\(18) & !\inst4|ADD[17]~67\)) # (!\inst4|SUMB\(18) & ((\inst4|SUMA\(18)) # (!\inst4|ADD[17]~67\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMB\(18),
	datab => \inst4|SUMA\(18),
	datad => VCC,
	cin => \inst4|ADD[17]~67\,
	combout => \inst4|ADD[18]~68_combout\,
	cout => \inst4|ADD[18]~69\);

-- Location: LCCOMB_X13_Y10_N6
\inst4|ADD[19]~70\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[19]~70_combout\ = (\inst4|SUMB\(19) & ((\inst4|SUMA\(19) & (!\inst4|ADD[18]~69\)) # (!\inst4|SUMA\(19) & ((\inst4|ADD[18]~69\) # (GND))))) # (!\inst4|SUMB\(19) & ((\inst4|SUMA\(19) & (\inst4|ADD[18]~69\ & VCC)) # (!\inst4|SUMA\(19) & 
-- (!\inst4|ADD[18]~69\))))
-- \inst4|ADD[19]~71\ = CARRY((\inst4|SUMB\(19) & ((!\inst4|ADD[18]~69\) # (!\inst4|SUMA\(19)))) # (!\inst4|SUMB\(19) & (!\inst4|SUMA\(19) & !\inst4|ADD[18]~69\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMB\(19),
	datab => \inst4|SUMA\(19),
	datad => VCC,
	cin => \inst4|ADD[18]~69\,
	combout => \inst4|ADD[19]~70_combout\,
	cout => \inst4|ADD[19]~71\);

-- Location: LCCOMB_X13_Y10_N8
\inst4|ADD[20]~72\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[20]~72_combout\ = ((\inst4|SUMA\(20) $ (\inst4|SUMB\(20) $ (\inst4|ADD[19]~71\)))) # (GND)
-- \inst4|ADD[20]~73\ = CARRY((\inst4|SUMA\(20) & ((!\inst4|ADD[19]~71\) # (!\inst4|SUMB\(20)))) # (!\inst4|SUMA\(20) & (!\inst4|SUMB\(20) & !\inst4|ADD[19]~71\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(20),
	datab => \inst4|SUMB\(20),
	datad => VCC,
	cin => \inst4|ADD[19]~71\,
	combout => \inst4|ADD[20]~72_combout\,
	cout => \inst4|ADD[20]~73\);

-- Location: LCCOMB_X13_Y10_N10
\inst4|ADD[21]~74\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[21]~74_combout\ = (\inst4|SUMA\(21) & ((\inst4|SUMB\(21) & (!\inst4|ADD[20]~73\)) # (!\inst4|SUMB\(21) & (\inst4|ADD[20]~73\ & VCC)))) # (!\inst4|SUMA\(21) & ((\inst4|SUMB\(21) & ((\inst4|ADD[20]~73\) # (GND))) # (!\inst4|SUMB\(21) & 
-- (!\inst4|ADD[20]~73\))))
-- \inst4|ADD[21]~75\ = CARRY((\inst4|SUMA\(21) & (\inst4|SUMB\(21) & !\inst4|ADD[20]~73\)) # (!\inst4|SUMA\(21) & ((\inst4|SUMB\(21)) # (!\inst4|ADD[20]~73\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(21),
	datab => \inst4|SUMB\(21),
	datad => VCC,
	cin => \inst4|ADD[20]~73\,
	combout => \inst4|ADD[21]~74_combout\,
	cout => \inst4|ADD[21]~75\);

-- Location: LCCOMB_X13_Y10_N12
\inst4|ADD[22]~76\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[22]~76_combout\ = ((\inst4|SUMA\(22) $ (\inst4|SUMB\(22) $ (\inst4|ADD[21]~75\)))) # (GND)
-- \inst4|ADD[22]~77\ = CARRY((\inst4|SUMA\(22) & ((!\inst4|ADD[21]~75\) # (!\inst4|SUMB\(22)))) # (!\inst4|SUMA\(22) & (!\inst4|SUMB\(22) & !\inst4|ADD[21]~75\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(22),
	datab => \inst4|SUMB\(22),
	datad => VCC,
	cin => \inst4|ADD[21]~75\,
	combout => \inst4|ADD[22]~76_combout\,
	cout => \inst4|ADD[22]~77\);

-- Location: LCCOMB_X13_Y10_N14
\inst4|ADD[23]~78\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[23]~78_combout\ = (\inst4|SUMA\(23) & ((\inst4|SUMB\(23) & (!\inst4|ADD[22]~77\)) # (!\inst4|SUMB\(23) & (\inst4|ADD[22]~77\ & VCC)))) # (!\inst4|SUMA\(23) & ((\inst4|SUMB\(23) & ((\inst4|ADD[22]~77\) # (GND))) # (!\inst4|SUMB\(23) & 
-- (!\inst4|ADD[22]~77\))))
-- \inst4|ADD[23]~79\ = CARRY((\inst4|SUMA\(23) & (\inst4|SUMB\(23) & !\inst4|ADD[22]~77\)) # (!\inst4|SUMA\(23) & ((\inst4|SUMB\(23)) # (!\inst4|ADD[22]~77\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(23),
	datab => \inst4|SUMB\(23),
	datad => VCC,
	cin => \inst4|ADD[22]~77\,
	combout => \inst4|ADD[23]~78_combout\,
	cout => \inst4|ADD[23]~79\);

-- Location: LCCOMB_X13_Y10_N16
\inst4|ADD[24]~80\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[24]~80_combout\ = ((\inst4|SUMA\(24) $ (\inst4|SUMB\(24) $ (\inst4|ADD[23]~79\)))) # (GND)
-- \inst4|ADD[24]~81\ = CARRY((\inst4|SUMA\(24) & ((!\inst4|ADD[23]~79\) # (!\inst4|SUMB\(24)))) # (!\inst4|SUMA\(24) & (!\inst4|SUMB\(24) & !\inst4|ADD[23]~79\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(24),
	datab => \inst4|SUMB\(24),
	datad => VCC,
	cin => \inst4|ADD[23]~79\,
	combout => \inst4|ADD[24]~80_combout\,
	cout => \inst4|ADD[24]~81\);

-- Location: LCCOMB_X13_Y10_N18
\inst4|ADD[25]~82\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[25]~82_combout\ = (\inst4|SUMB\(25) & ((\inst4|SUMA\(25) & (!\inst4|ADD[24]~81\)) # (!\inst4|SUMA\(25) & ((\inst4|ADD[24]~81\) # (GND))))) # (!\inst4|SUMB\(25) & ((\inst4|SUMA\(25) & (\inst4|ADD[24]~81\ & VCC)) # (!\inst4|SUMA\(25) & 
-- (!\inst4|ADD[24]~81\))))
-- \inst4|ADD[25]~83\ = CARRY((\inst4|SUMB\(25) & ((!\inst4|ADD[24]~81\) # (!\inst4|SUMA\(25)))) # (!\inst4|SUMB\(25) & (!\inst4|SUMA\(25) & !\inst4|ADD[24]~81\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMB\(25),
	datab => \inst4|SUMA\(25),
	datad => VCC,
	cin => \inst4|ADD[24]~81\,
	combout => \inst4|ADD[25]~82_combout\,
	cout => \inst4|ADD[25]~83\);

-- Location: LCCOMB_X13_Y10_N20
\inst4|ADD[26]~84\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[26]~84_combout\ = ((\inst4|SUMB\(26) $ (\inst4|SUMA\(26) $ (\inst4|ADD[25]~83\)))) # (GND)
-- \inst4|ADD[26]~85\ = CARRY((\inst4|SUMB\(26) & (\inst4|SUMA\(26) & !\inst4|ADD[25]~83\)) # (!\inst4|SUMB\(26) & ((\inst4|SUMA\(26)) # (!\inst4|ADD[25]~83\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMB\(26),
	datab => \inst4|SUMA\(26),
	datad => VCC,
	cin => \inst4|ADD[25]~83\,
	combout => \inst4|ADD[26]~84_combout\,
	cout => \inst4|ADD[26]~85\);

-- Location: LCCOMB_X13_Y10_N22
\inst4|ADD[27]~86\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[27]~86_combout\ = (\inst4|SUMB\(27) & ((\inst4|SUMA\(27) & (!\inst4|ADD[26]~85\)) # (!\inst4|SUMA\(27) & ((\inst4|ADD[26]~85\) # (GND))))) # (!\inst4|SUMB\(27) & ((\inst4|SUMA\(27) & (\inst4|ADD[26]~85\ & VCC)) # (!\inst4|SUMA\(27) & 
-- (!\inst4|ADD[26]~85\))))
-- \inst4|ADD[27]~87\ = CARRY((\inst4|SUMB\(27) & ((!\inst4|ADD[26]~85\) # (!\inst4|SUMA\(27)))) # (!\inst4|SUMB\(27) & (!\inst4|SUMA\(27) & !\inst4|ADD[26]~85\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMB\(27),
	datab => \inst4|SUMA\(27),
	datad => VCC,
	cin => \inst4|ADD[26]~85\,
	combout => \inst4|ADD[27]~86_combout\,
	cout => \inst4|ADD[27]~87\);

-- Location: LCCOMB_X13_Y10_N24
\inst4|ADD[28]~88\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[28]~88_combout\ = ((\inst4|SUMB\(28) $ (\inst4|SUMA\(28) $ (\inst4|ADD[27]~87\)))) # (GND)
-- \inst4|ADD[28]~89\ = CARRY((\inst4|SUMB\(28) & (\inst4|SUMA\(28) & !\inst4|ADD[27]~87\)) # (!\inst4|SUMB\(28) & ((\inst4|SUMA\(28)) # (!\inst4|ADD[27]~87\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMB\(28),
	datab => \inst4|SUMA\(28),
	datad => VCC,
	cin => \inst4|ADD[27]~87\,
	combout => \inst4|ADD[28]~88_combout\,
	cout => \inst4|ADD[28]~89\);

-- Location: LCCOMB_X13_Y10_N26
\inst4|ADD[29]~90\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[29]~90_combout\ = (\inst4|SUMA\(29) & ((\inst4|SUMB\(29) & (!\inst4|ADD[28]~89\)) # (!\inst4|SUMB\(29) & (\inst4|ADD[28]~89\ & VCC)))) # (!\inst4|SUMA\(29) & ((\inst4|SUMB\(29) & ((\inst4|ADD[28]~89\) # (GND))) # (!\inst4|SUMB\(29) & 
-- (!\inst4|ADD[28]~89\))))
-- \inst4|ADD[29]~91\ = CARRY((\inst4|SUMA\(29) & (\inst4|SUMB\(29) & !\inst4|ADD[28]~89\)) # (!\inst4|SUMA\(29) & ((\inst4|SUMB\(29)) # (!\inst4|ADD[28]~89\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMA\(29),
	datab => \inst4|SUMB\(29),
	datad => VCC,
	cin => \inst4|ADD[28]~89\,
	combout => \inst4|ADD[29]~90_combout\,
	cout => \inst4|ADD[29]~91\);

-- Location: LCCOMB_X13_Y10_N28
\inst4|ADD[30]~92\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[30]~92_combout\ = ((\inst4|SUMB\(30) $ (\inst4|SUMA\(30) $ (\inst4|ADD[29]~91\)))) # (GND)
-- \inst4|ADD[30]~93\ = CARRY((\inst4|SUMB\(30) & (\inst4|SUMA\(30) & !\inst4|ADD[29]~91\)) # (!\inst4|SUMB\(30) & ((\inst4|SUMA\(30)) # (!\inst4|ADD[29]~91\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMB\(30),
	datab => \inst4|SUMA\(30),
	datad => VCC,
	cin => \inst4|ADD[29]~91\,
	combout => \inst4|ADD[30]~92_combout\,
	cout => \inst4|ADD[30]~93\);

-- Location: LCCOMB_X13_Y10_N30
\inst4|ADD[31]~94\ : cycloneive_lcell_comb
-- Equation(s):
-- \inst4|ADD[31]~94_combout\ = \inst4|SUMB\(31) $ (\inst4|ADD[30]~93\ $ (!\inst4|SUMA\(31)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \inst4|SUMB\(31),
	datad => \inst4|SUMA\(31),
	cin => \inst4|ADD[30]~93\,
	combout => \inst4|ADD[31]~94_combout\);

-- Location: FF_X13_Y10_N31
\inst4|ADD[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[31]~94_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(31));

-- Location: FF_X13_Y10_N29
\inst4|ADD[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[30]~92_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(30));

-- Location: FF_X13_Y10_N27
\inst4|ADD[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[29]~90_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(29));

-- Location: FF_X13_Y10_N25
\inst4|ADD[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[28]~88_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(28));

-- Location: FF_X13_Y10_N23
\inst4|ADD[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[27]~86_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(27));

-- Location: FF_X13_Y10_N21
\inst4|ADD[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[26]~84_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(26));

-- Location: FF_X13_Y10_N19
\inst4|ADD[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[25]~82_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(25));

-- Location: FF_X13_Y10_N17
\inst4|ADD[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[24]~80_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(24));

-- Location: FF_X13_Y10_N15
\inst4|ADD[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[23]~78_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(23));

-- Location: FF_X13_Y10_N13
\inst4|ADD[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[22]~76_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(22));

-- Location: FF_X13_Y10_N11
\inst4|ADD[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[21]~74_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(21));

-- Location: FF_X13_Y10_N9
\inst4|ADD[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[20]~72_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(20));

-- Location: FF_X13_Y10_N7
\inst4|ADD[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[19]~70_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(19));

-- Location: FF_X13_Y10_N5
\inst4|ADD[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[18]~68_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(18));

-- Location: FF_X13_Y10_N3
\inst4|ADD[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[17]~66_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(17));

-- Location: FF_X13_Y10_N1
\inst4|ADD[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[16]~64_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(16));

-- Location: FF_X13_Y11_N31
\inst4|ADD[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[15]~62_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(15));

-- Location: FF_X13_Y11_N29
\inst4|ADD[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[14]~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(14));

-- Location: FF_X13_Y11_N27
\inst4|ADD[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[13]~58_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(13));

-- Location: FF_X13_Y11_N25
\inst4|ADD[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[12]~56_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(12));

-- Location: FF_X13_Y11_N23
\inst4|ADD[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[11]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(11));

-- Location: FF_X13_Y11_N21
\inst4|ADD[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[10]~52_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(10));

-- Location: FF_X13_Y11_N19
\inst4|ADD[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[9]~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(9));

-- Location: FF_X13_Y11_N17
\inst4|ADD[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[8]~48_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(8));

-- Location: FF_X13_Y11_N15
\inst4|ADD[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[7]~46_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(7));

-- Location: FF_X13_Y11_N13
\inst4|ADD[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[6]~44_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(6));

-- Location: FF_X13_Y11_N11
\inst4|ADD[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[5]~42_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(5));

-- Location: FF_X13_Y11_N9
\inst4|ADD[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[4]~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(4));

-- Location: FF_X13_Y11_N7
\inst4|ADD[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[3]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(3));

-- Location: FF_X13_Y11_N5
\inst4|ADD[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[2]~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(2));

-- Location: FF_X13_Y11_N3
\inst4|ADD[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[1]~34_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(1));

-- Location: FF_X13_Y11_N1
\inst4|ADD[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \inst7|DIVD~clkctrl_outclk\,
	d => \inst4|ADD[0]~32_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \inst4|ADD\(0));

ww_RESET <= \RESET~output_o\;

ww_POWER(31) <= \POWER[31]~output_o\;

ww_POWER(30) <= \POWER[30]~output_o\;

ww_POWER(29) <= \POWER[29]~output_o\;

ww_POWER(28) <= \POWER[28]~output_o\;

ww_POWER(27) <= \POWER[27]~output_o\;

ww_POWER(26) <= \POWER[26]~output_o\;

ww_POWER(25) <= \POWER[25]~output_o\;

ww_POWER(24) <= \POWER[24]~output_o\;

ww_POWER(23) <= \POWER[23]~output_o\;

ww_POWER(22) <= \POWER[22]~output_o\;

ww_POWER(21) <= \POWER[21]~output_o\;

ww_POWER(20) <= \POWER[20]~output_o\;

ww_POWER(19) <= \POWER[19]~output_o\;

ww_POWER(18) <= \POWER[18]~output_o\;

ww_POWER(17) <= \POWER[17]~output_o\;

ww_POWER(16) <= \POWER[16]~output_o\;

ww_POWER(15) <= \POWER[15]~output_o\;

ww_POWER(14) <= \POWER[14]~output_o\;

ww_POWER(13) <= \POWER[13]~output_o\;

ww_POWER(12) <= \POWER[12]~output_o\;

ww_POWER(11) <= \POWER[11]~output_o\;

ww_POWER(10) <= \POWER[10]~output_o\;

ww_POWER(9) <= \POWER[9]~output_o\;

ww_POWER(8) <= \POWER[8]~output_o\;

ww_POWER(7) <= \POWER[7]~output_o\;

ww_POWER(6) <= \POWER[6]~output_o\;

ww_POWER(5) <= \POWER[5]~output_o\;

ww_POWER(4) <= \POWER[4]~output_o\;

ww_POWER(3) <= \POWER[3]~output_o\;

ww_POWER(2) <= \POWER[2]~output_o\;

ww_POWER(1) <= \POWER[1]~output_o\;

ww_POWER(0) <= \POWER[0]~output_o\;
END structure;


