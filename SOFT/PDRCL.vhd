Library ieee; 
use ieee.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;

use ieee.numeric_std.all;
--use ieee.std_logic_arith.all; 
--use ieee.std_logic_signed.all;

--Propagation delay reference combining logic
 ENTITY PDRCL IS
 generic(N: integer :=32);--Number of bits of refecence combinator
 PORT (REF: IN std_logic_vector(N-1 downto 0);
		MEA: IN std_logic_vector(N-1 downto 0);
		PROP: OUT std_logic_vector(N-1 downto 0)
 );
 END ENTITY;

ARCHITECTURE PROP_DELAY_TIMER OF PDRCL IS
signal RESULT : std_logic_vector(N-1 downto 0);--Signal timer

BEGIN

--NOTDELAYS
NOTDELAYS : for i in N-1 downto 0 generate
    RESULT(i) <=  REF(i) XOR MEA(i);
end generate; 

PROP<=RESULT;

END ARCHITECTURE;