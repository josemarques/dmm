LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY BSACC IS --Bit stream accumulator
	GENERIC (NBS: integer := 32;--Number of bits in the bits stream
				NBP: integer := 12;--Number of bits of preliminar accumulator
				NBA:integer:=32--Number of bits of the accumulator
	);
  
	PORT(CLK:IN std_logic;	
	BITSTR: IN std_logic_vector (NBS-1 DOWNTO 0);
			RESET: IN std_logic;
			
			ADD: OUT std_logic_vector (NBA-1 DOWNTO 0)
	);

			END BSACC;

ARCHITECTURE BINARYCNT OF BSACC IS
  ATTRIBUTE keep: BOOLEAN;
  ATTRIBUTE preserve: BOOLEAN;
  
  --TYPE std_logic_vector_2 IS ARRAY (NBS-2 DOWNTO 0) OF std_logic_vector(NBA-1 DOWNTO 0);
  --TYPE PSUMMTX IS ARRAY (NBS-1 DOWNTO 0) OF std_logic_vector(NBS-1 DOWNTO 0);
  TYPE PSUMMTX IS ARRAY ((NBS/2)-1 DOWNTO 0) OF std_logic_vector(NBP-1 DOWNTO 0);
  TYPE TSUMMTX IS ARRAY ((NBS/2)-2 DOWNTO 0) OF std_logic_vector(NBA-1 DOWNTO 0);
 
  SIGNAL PSUM: PSUMMTX;--Partial sum
  SIGNAL TSUM: TSUMMTX;--Temporal sum for full sum calculation
  
  SIGNAL SUM: std_logic_vector (NBA-1 DOWNTO 0):=(OTHERS=>'0');
  
  
  --SIGNAL SC: std_logic_vector (NBS-1 DOWNTO 0):=(OTHERS=>'0');--Stream capture
	
ATTRIBUTE keep of PSUM: signal is true;
 ATTRIBUTE keep of TSUM: signal is true;
 ATTRIBUTE keep of SUM: signal is true;
 --ATTRIBUTE keep of SC: signal is true;
 ATTRIBUTE preserve OF PSUM: SIGNAL IS true;
ATTRIBUTE preserve OF SUM: SIGNAL IS true;
ATTRIBUTE preserve OF TSUM: SIGNAL IS true;
--ATTRIBUTE preserve OF SC: SIGNAL IS true;
  
BEGIN
	
	TSUM(0)<=std_logic_vector(resize(unsigned(PSUM(0))+unsigned(PSUM(1)),TSUM(0)'LENGTH));
	SUMCALC : FOR s IN 1 TO (NBS/2)-2 GENERATE
		TSUM(s)<=std_logic_vector(unsigned(TSUM(s-1))+unsigned(PSUM(s+1)));
	END GENERATE; 
	
	SUM<=TSUM((NBS/2)-2);
	--SUM<=TSUM(2);

	--RESULTCALC : FOR a IN 0 TO NBA-1 GENERATE
    --ADD(a)<=SUM(a);
	 --ADD(a)<='1';
	--END GENERATE; 

	ADD<= std_logic_vector(resize(signed(SUM),ADD'length));
	--ADD(NBA-1)<='1';
		--ADD(NBA-1)<='1';
--ADD<=std_logic_vector(to_signed(-12345, ADD'length));

PROCESS(CLK)--Is possible that this dont trigger every time
-- VARIABLE SC :std_logic_vector (NBS-1 DOWNTO 0):=(OTHERS=>'0');--Stream capture
		BEGIN
	IF (CLK'EVENT AND CLK= '1') THEN
	PARTIALSUMS : FOR s in (NBS/2)-1 DOWNTO 0 LOOP
		IF(RESET='1') THEN
				PSUM(s)<= (OTHERS =>'0');
			ELSIF(BITSTR(s*2)='1') THEN
			PSUM(s)<=std_logic_vector(unsigned(PSUM(s))+1);
		END IF;
		END LOOP;
END IF;
END PROCESS;


END BINARYCNT;