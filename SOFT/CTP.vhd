--CTP--Clock signal to pulse--
Library ieee; 
use ieee.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;
--use ieee.std_logic_arith.all; 
--use ieee.std_logic_signed.all;
entity CTP is
	port(	CLK : IN std_logic;--System clock
			SCLK : IN std_logic;--Slow clock; Divided clock
			SPCLK : OUT std_logic:='0'--Output slow pulsed clock
	    );
end CTP;
--

architecture PULSED_CLOCK_ARCH of CTP is
signal DVCLK: std_logic:='0';--DIVIDED CLK
SIGNAL COUNT: std_logic_vector (2 DOWNTO 0):=(OTHERS=>'0');

signal PCLK: std_logic:='0';--PREVIOUS CLK
signal PSCLK: std_logic:='0';--PREVIOUS SLOW CLK
signal BSPCLK: std_logic:='0';--Buffered output slow pulsed clock

BEGIN
SPCLK<=BSPCLK;

PROCESS (CLK)
  BEGIN
    IF (CLK = '1' AND CLK'EVENT) THEN
        COUNT  <= std_logic_vector(unsigned(COUNT)+1);
		  DVCLK<=COUNT(0);
		  --DVCLK<=COUNT(1);
      END IF;
  END PROCESS;

PROCESS (DVCLK) 
BEGIN
	IF (DVCLK'EVENT AND DVCLK='1') THEN
				
		IF (SCLK='1' AND PSCLK='0') THEN
			BSPCLK<='1';
			PSCLK<='1';
		
		ELSIF(SCLK='0' AND PSCLK='1') THEN
			--BSPCLK<='1';
			PSCLK<='0';
		
		ELSIF (BSPCLK='1')THEN
			BSPCLK<='0';
			
		END IF;
		
	END IF;
	
END PROCESS;

end PULSED_CLOCK_ARCH;