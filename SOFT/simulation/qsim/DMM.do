onerror {exit -code 1}
vlib work
vcom -work work DMM.vho
vcom -work work Waveform.vwf.vht
vsim -c -t 1ps -sdfmax DMM_vhd_vec_tst/i1=DMM_vhd.sdo -L cycloneive -L altera -L altera_mf -L 220model -L sgate -L altera_lnsim work.DMM_vhd_vec_tst
vcd file -direction DMM.msim.vcd
vcd add -internal DMM_vhd_vec_tst/*
vcd add -internal DMM_vhd_vec_tst/i1/*
proc simTimestamp {} {
    echo "Simulation time: $::now ps"
    if { [string equal running [runStatus]] } {
        after 2500 simTimestamp
    }
}
after 2500 simTimestamp
run -all
quit -f




